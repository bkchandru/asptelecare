﻿using ASPTelecare.Common;
using ASPTelecare.Common.Business;
using ASPTelecare.Contract.Business.Common;
using ASPTelecare.Contract.Data.Common;
using ASPTelecare.Entity.Common;
using ASPTelecare.ViewModel.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ASPTelecare.Business.Patient
{
    public class ListMasterManager : CurdManager<ListMaster, ListMasterViewModel, int>, IListMasterManager
    {
        private readonly IListMasterRepo _repo;
        public ListMasterManager(IListMasterRepo repo) 
            : base(repo)
        {
            _repo = repo;
        }

        public async Task<List<ListMasterViewModel>> GetByListTypeAsync(ListType listType)
        {
            return await _repo.GetByListTypeAsync(listType);
        }
    }
}
