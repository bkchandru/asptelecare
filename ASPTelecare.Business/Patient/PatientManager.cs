﻿using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using ASPTelecare.Common.Business;
using ASPTelecare.Contract.Business.Patient;
using ASPTelecare.Contract.Data.Patient;
using ASPTelecare.ViewModel.Patient;
using Microsoft.Extensions.Logging;

namespace ASPTelecare.Business.Patient
{
    public class PatientManager: CurdManager<Entity.Patient, PatientViewModel, int>, IPatientManager
    {
        private readonly IPatientRepo _repo;
        private readonly ILogger<PatientManager> _logger;
        public PatientManager(IPatientRepo repo, ILogger<PatientManager> logger) 
            : base(repo)
        {
            _repo = repo;
            _logger = logger;
        }

        public override Task UpdateAsync(PatientViewModel viewModel)
        {
            _logger.LogInformation("Patient Record is Updated");
            if (viewModel.FirstName == "Chandru")
            {
                _logger.LogCritical("Error Patient Record is Updated");
                throw new ValidationException("First Name should not be Chandru");
            }
            return base.UpdateAsync(viewModel);
        }
    }
}
