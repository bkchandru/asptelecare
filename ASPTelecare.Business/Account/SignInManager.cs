﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using ASPTelecare.Contract.Business.Account;
using ASPTelecare.Entity;
using ASPTelecare.Entity.Account;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ASPTelecare.Business.Account
{
    public class SignInManager : SignInManager<User>, ISignInManager<User>
    {
        public SignInManager(UserManager<User> userManager, 
            IHttpContextAccessor contextAccessor, 
            IUserClaimsPrincipalFactory<User> claimsFactory, 
            IOptions<IdentityOptions> optionsAccessor, ILogger<SignInManager<User>> logger, 
            IAuthenticationSchemeProvider schemes) :
            base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes)
        {
        }

        public override Task<SignInResult> PasswordSignInAsync(string userName, string password, bool isPersistent, bool lockoutOnFailure)
        {
            var result = new SignInResult();
            return Task.FromResult(result);
            //return base.PasswordSignInAsync(userName, password, isPersistent, lockoutOnFailure);
        }

        public override Task SignOutAsync()
        {
            return base.SignOutAsync();
        }
    }

    
}
