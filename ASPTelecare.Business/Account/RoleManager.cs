﻿using ASPTelecare.Contract.Business.Account;
using ASPTelecare.Entity;
using ASPTelecare.Entity.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASPTelecare.Business.Account
{
    public class RoleManager : RoleManager<Role>, IRoleManager<Role>
    {
        public RoleManager(IRoleStore<Role> store, 
            IEnumerable<IRoleValidator<Role>> roleValidators, 
            ILookupNormalizer keyNormalizer, 
            IdentityErrorDescriber errors, 
            ILogger<RoleManager<Role>> logger) 
            : base(store, roleValidators, keyNormalizer, errors, logger)
        {

        }
    }
}
