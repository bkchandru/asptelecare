﻿using ASPTelecare.Common.Helper;
using ASPTelecare.Contract.Business.Account;
using ASPTelecare.Entity;
using ASPTelecare.Entity.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Threading.Tasks;

namespace ASPTelecare.Business.Account
{
    public class UserManager : UserManager<User>, IUserManager<User>
    {
        public UserManager(IUserStore<User> store, 
            IOptions<IdentityOptions> optionsAccessor, 
            IPasswordHasher<User> passwordHasher, 
            IEnumerable<IUserValidator<User>> userValidators, 
            IEnumerable<IPasswordValidator<User>> passwordValidators, 
            ILookupNormalizer keyNormalizer, 
            IdentityErrorDescriber errors, 
            IServiceProvider services, 
            ILogger<UserManager<User>> logger) 
            :base(store, optionsAccessor, passwordHasher, userValidators,
                  passwordValidators, keyNormalizer, errors, services, logger)
        {

        }

        public override async Task<IdentityResult> CreateAsync(User user, string password)
        {
            user.Email = user.UserName;
            var result = await base.CreateAsync(user, password);
            if (result.Succeeded)
            {
                var code = await GenerateEmailConfirmationTokenAsync(user);
                var callbackUrl = $"{ConfigurationManager.AppSetting.WebHostAddress}/Account/ConfirmEmail/?userId={user.Id}&code={code}";
                //TODO: Send Email  
                //await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                //       $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");
            }
            return result;
        }
    }
}
