﻿using ASPTelecare.Common.Helper;
using ASPTelecare.Entity;
using ASPTelecare.Entity.Account;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ASPTelecare.Data
{
    public class AccountDbContext : IdentityDbContext<User,Role,int,UserClaim,UserRole,UserLogin,RoleClaim,UserToken>
    {

        #region Db Sets

        #region Account

        public virtual DbSet<AccountPolicy> AccountPolicies { get; set; }
        public virtual DbSet<Privilege> Privileges { get; set; }
        public virtual DbSet<RolePrivilege> RolePrivileges { get; set; }

        #endregion Account

        #endregion Db Sets

        #region Constructor & Destructor
        public AccountDbContext(DbContextOptions<AccountDbContext> options)
            : base(options)
        {
            //Database.EnsureCreated();
        }

        #endregion Constructor & Destructor

        #region Configuration
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.AppSetting.DbConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<User>().ToTable("User").Property(e => e.Id).HasColumnName("UserId");
            modelBuilder.Entity<Role>().ToTable("Role").Property(e => e.Id).HasColumnName("RoleId");
            modelBuilder.Entity<UserRole>().ToTable("UserRole");
            modelBuilder.Entity<UserLogin>().ToTable("UserLogin");
            modelBuilder.Entity<UserToken>().ToTable("UserToken");
            modelBuilder.Entity<UserClaim>().ToTable("UserClaim").Property(e => e.Id).HasColumnName("UserClaimId");
            modelBuilder.Entity<RoleClaim>().ToTable("RoleClaim").Property(e => e.Id).HasColumnName("RoleClaimId");
            modelBuilder.Entity<RolePrivilege>().HasKey(o => new { o.RoleId, o.PrivilegeId });
        }

        #endregion Configuration

    }
}
