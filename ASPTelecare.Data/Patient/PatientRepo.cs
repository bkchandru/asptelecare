﻿using System.Linq;
using System.Threading.Tasks;
using ASPTelecare.Common.Data;
using ASPTelecare.Common.Helper;
using ASPTelecare.Common.ViewModel;
using ASPTelecare.Contract.Data.Patient;
using ASPTelecare.ViewModel.Patient;
using Microsoft.EntityFrameworkCore;

namespace ASPTelecare.Data.Patient
{
    public class PatientRepo: CurdRepo<Entity.Patient, PatientViewModel, int>, IPatientRepo
    {
        private readonly AppDbContext _dbContext;
        public PatientRepo(AppDbContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public override async Task<PagingList<object>> GetAllPagedAsync(PagingDetails pagingDetails)
        {
            var query = _dbContext.Patients.AsNoTracking()
            .Select(o => new PatientGridViewModel()
            {
                Id = o.Id,
                FirstName = o.FirstName,
                LastName = o.LastName,
                Gender = o.Gender,
                BloodGroup = o.BloodGroup,
                MobileNumber = o.MobileNumber,
                EmailId = o.EmailId
            });
            if (!string.IsNullOrEmpty(pagingDetails.Filter))
            {
                query = query.Where(o => o.FirstName.Contains(pagingDetails.Filter)
                || o.LastName.Contains(pagingDetails.Filter)
                || o.Gender.Contains(pagingDetails.Filter)
                || o.BloodGroup.Contains(pagingDetails.Filter)
                || o.MobileNumber.Contains(pagingDetails.Filter)
                || o.EmailId.Contains(pagingDetails.Filter));
            }
            return await query.ToPagingListAsync<object>(pagingDetails);
        }

    }
}
