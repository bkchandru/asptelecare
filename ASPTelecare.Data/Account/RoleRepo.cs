﻿using ASPTelecare.Contract.Data.Account;
using ASPTelecare.Entity;
using ASPTelecare.Entity.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASPTelecare.Data.Account
{
    public class RoleRepo : RoleStore<Role, AccountDbContext, int, UserRole, RoleClaim>
        ,IRoleRepo<Role, AccountDbContext, int, UserRole, RoleClaim>
    {
        public RoleRepo(AccountDbContext context, IdentityErrorDescriber describer = null) 
            : base(context, describer)
        {

        }
    }
}
