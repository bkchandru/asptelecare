﻿using ASPTelecare.Contract.Data.Account;
using ASPTelecare.Entity;
using ASPTelecare.Entity.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASPTelecare.Data.Account
{
    public class UserRepo: UserStore<User, Role,AccountDbContext, int, UserClaim, UserRole, UserLogin, UserToken, RoleClaim>
        ,IUserRepo<User, Role, AccountDbContext, int, UserClaim, UserRole, UserLogin, UserToken, RoleClaim>
    {
        public UserRepo(AccountDbContext context, IdentityErrorDescriber describer = null)
            :base(context, describer)
        { }
    }
}
