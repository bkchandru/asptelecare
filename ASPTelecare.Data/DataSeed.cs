﻿using ASPTelecare.Common;
using ASPTelecare.Data.Account;
using ASPTelecare.Entity.Account;
using ASPTelecare.Entity.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASPTelecare.Data
{
    public static class DataSeed
    {
        public static void Initialize(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<ListMaster>().HasData(
             new ListMaster { Id = 1, ListType = ListType.Gender, Value ="M", Text="Male", InstituteId=1  },
             new ListMaster { Id = 2, ListType = ListType.Gender, Value = "F", Text = "FeMale", InstituteId = 1 },
             new ListMaster { Id = 3, ListType = ListType.Gender, Value = "O", Text = "Other", InstituteId = 1 },
             new ListMaster { Id = 4, ListType = ListType.BloodGroup, Value = "A", Text = "A", InstituteId = 1 },
             new ListMaster { Id = 5, ListType = ListType.BloodGroup, Value = "B", Text = "B", InstituteId = 1 },
             new ListMaster { Id = 6, ListType = ListType.BloodGroup, Value = "B+", Text = "B +Ve", InstituteId = 1 },
             new ListMaster { Id = 7, ListType = ListType.BloodGroup, Value = "C", Text = "C", InstituteId = 1 },
             new ListMaster { Id = 8, ListType = ListType.BloodGroup, Value = "D", Text = "D", InstituteId = 1 },
             new ListMaster { Id = 9, ListType = ListType.SmokingStatus, Value = "S", Text = "Smooking", InstituteId = 1 },
             new ListMaster { Id = 10, ListType = ListType.SmokingStatus, Value = "N", Text = "Non Smooking", InstituteId = 1 },
             new ListMaster { Id = 11, ListType = ListType.Education, Value = "BE", Text = "Batchelor of Engeenearing", InstituteId = 1 },
             new ListMaster { Id = 12, ListType = ListType.Education, Value = "BS", Text = "Batchelor of Science", InstituteId = 1 }
          );

            modelBuilder.Entity<Privilege>().HasData(
              new Privilege { Id = 1, ParentPrivilegeId = 1, Name = "Admin", Description = "Admin Module", Level = 1, DisplayOrder = 1, Secured = true, ApplicationId = 1, Active = true },
              new Privilege { Id = 2, ParentPrivilegeId = 1, Name = "Institute", Description = "Institute Page", Level = 2, DisplayOrder = 4, Secured = true, ApplicationId = 1, Active = true },
              new Privilege { Id = 3, ParentPrivilegeId = 1, Name = "Role", Description = "Role Page", Level = 2, DisplayOrder = 2, Secured = true, ApplicationId = 1, Active = true },
              new Privilege { Id = 4, ParentPrivilegeId = 1, Name = "User", Description = "User Page", Level = 2, DisplayOrder = 3, Secured = true, ApplicationId = 1, Active = true }
              
           );

            modelBuilder.Entity<Role>().HasData(
               new Role { Id = 1, Name = "SuperAdmin", NormalizedName = "ADMIN", InstituteId = 1,AddedBy = "chandru", AddedDate = DateTime.UtcNow },
               new Role { Id = 2, Name = "InstituteAdmin", NormalizedName = "INSTITUTEADMIN", InstituteId = 1, AddedBy = "chandru", AddedDate = DateTime.UtcNow },
               new Role { Id = 3, Name = "InstituteUser", NormalizedName = "INSTITUTEUSER", InstituteId = 1, AddedBy = "chandru", AddedDate = DateTime.UtcNow }
            );

            modelBuilder.Entity<RolePrivilege>().HasData(
                new RolePrivilege { RoleId = 1, PrivilegeId = 1 },
                new RolePrivilege { RoleId = 1, PrivilegeId = 2 },
                new RolePrivilege { RoleId = 1, PrivilegeId = 3 },
                new RolePrivilege { RoleId = 1, PrivilegeId = 4 }
            );

            modelBuilder.Entity<User>().HasData(
                new User { Id = 1, FirstName = "Guru", LastName = "BN",  DisplayName="Guru BN", Email= "gurubn@hitforward.com", UserName= "gurubn@hitforward.com", EmailConfirmed=true, NormalizedEmail= "GURUBN@HITFORWARD.COM", NormalizedUserName= "GURUBN@HITFORWARD.COM", PasswordHash= "AQAAAAEAACcQAAAAEDpHvkVmk6oKZYRTZ/2Wt4tWBfizyfb7F1TOCZQUQoEMUXbBj8IAEO5PmZl2XHZqcw==", PhoneNumber="7676767676", PhoneNumberConfirmed=true, InstituteId=1, Approved=true, Active=true, AddedBy = "gurubn@hitforward.com", AddedDate = DateTime.UtcNow }
            );
            
            modelBuilder.Entity<UserRole>().HasData(
                new UserRole { RoleId=1,UserId=1 }
            );

            modelBuilder.Entity<Entity.Patient>().HasData(
                new Entity.Patient {Id=1, FirstName = "First Name 1", LastName = "Last Name 1", Gender = "M", MRN="MMMM", MobileNumber = "9787878787", EmailId = "ccca@hhhh.com", AddedBy = "chandru", AddedDate = DateTime.UtcNow },
                new Entity.Patient { Id = 2, FirstName = "First Name 2", LastName = "Last Name 2", Gender = "F", MRN = "NNN", MobileNumber = "9787878787", EmailId = "ccca@hhhh.com", AddedBy = "chandru", AddedDate = DateTime.UtcNow }
            );
        }
    }
}
