﻿using ASPTelecare.Common.Helper;
using ASPTelecare.Entity;
using ASPTelecare.Entity.Account;
using ASPTelecare.Entity.Common;
using Microsoft.EntityFrameworkCore;

namespace ASPTelecare.Data
{
    public class AppDbContext : AccountDbContext
    {

        #region Db Sets
        public virtual DbSet<AppLog> AppLogs { get; set; }
        public virtual DbSet<ListMaster> ListMasters { get; set; }
        public virtual DbSet<Institution> Institutions { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<Entity.Patient> Patients { get; set; }
        public virtual DbSet<ProviderPatient> ProviderPatients { get; set; }

        #endregion Db Sets

        #region Constructor & Destructor
        public AppDbContext(DbContextOptions<AccountDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        #endregion Constructor & Destructor

        #region Configuration
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //if (!optionsBuilder.IsConfigured)
            //{
            optionsBuilder.UseSqlServer(ConfigurationManager.AppSetting.DbConnectionString);
            //}
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ProviderPatient>().HasKey(o => new { o.ProviderId, o.PatientId });

            DataSeed.Initialize(modelBuilder);
        }

        #endregion Configuration

    }
}
