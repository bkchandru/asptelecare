﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASPTelecare.Common;
using ASPTelecare.Common.Data;
using ASPTelecare.Common.Helper;
using ASPTelecare.Common.ViewModel;
using ASPTelecare.Contract.Data.Common;
using ASPTelecare.Contract.Data.Patient;
using ASPTelecare.Entity.Common;
using ASPTelecare.ViewModel.Common;
using ASPTelecare.ViewModel.Patient;
using Microsoft.EntityFrameworkCore;

namespace ASPTelecare.Data.Patient
{
    public class ListMasterRepo : CurdRepo<ListMaster, ListMasterViewModel, int>, IListMasterRepo
    {
        private readonly AppDbContext _dbContext;
        public ListMasterRepo(AppDbContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<ListMasterViewModel>> GetByListTypeAsync(ListType listType)
        {
           var result =  await _dbContext.ListMasters.AsNoTracking()
                .Where(o => o.ListType == listType)
                .Select(o => new ListMasterViewModel()
                {
                    Id=o.Id,
                    Value = o.Value,
                    Text = o.Text
                }).ToListAsync();
            return result;
        }
    }
}
