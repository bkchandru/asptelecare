﻿namespace ASPTelecare.Common
{
    public enum UserType
    {
        SuperAdmin,
        InstitutionAdmin,
        InstitutionUser,
        Provider,
        Patient
    }

    public enum AddressType
    {
        Home,
        Work,
        Other,
    }

    public enum ListType
    {
        Gender,
        SmokingStatus,
        BloodGroup,
        Education
    }
}
