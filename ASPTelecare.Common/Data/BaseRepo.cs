﻿using ASPTelecare.Common.Data.Contract;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASPTelecare.Common.Data
{
    public class BaseRepo : IBaseRepo
    {
        private readonly DbContext _dbContext;

        #region Constructor & Destructor

        public BaseRepo(DbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException("dbContext");
        }

        public void Dispose()
        {
        }

        #endregion Constructor & Destructor

        public virtual async Task<int> SaveAync()
        {
            if (_dbContext != null)
            {
                return await _dbContext.SaveChangesAsync();
            }

            return 0;
        }
    }
}
