﻿using ASPTelecare.Common.Data.Contract;
using ASPTelecare.Common.Helper;
using ASPTelecare.Common.ViewModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ASPTelecare.Common.Data
{
    public abstract class CurdRepo<TDbEntity, TViewModel, TColumnDataType>
        :BaseRepo, ICurdRepo<TDbEntity, TViewModel, TColumnDataType>
        where TDbEntity : class, new()
        where TViewModel : class, new()
    {
        protected DbSet<TDbEntity> _table;
        private readonly DbContext _dbContext;

        protected CurdRepo(DbContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(dbContext.GetType().Name);
            _dbContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.TrackAll;
            _table = _dbContext.Set<TDbEntity>();

        }

        public virtual async Task<IEnumerable<TViewModel>> GetAllAsync()
        {
            var query = _table.AsNoTracking().AsQueryable();
            var entities = await query.ToListAsync();
            var models = new List<TViewModel>();
            foreach (var entity in entities)
            {
                var model = MapEntityToViewModel(entity);
                models.Add(model);
            }
            return models.AsEnumerable();
        }

        public virtual async Task<PagingList<object>> GetAllPagedAsync(PagingDetails pagingDetails)
        {
            IQueryable<object> query = _table.AsNoTracking();
            query = query.FullTextFilter(pagingDetails.Filter);
            return await query.ToPagingListAsync(pagingDetails);
        }

        public virtual async Task<TViewModel> GetByIdAsync(TColumnDataType id)
        {
            var entity = await _table.FindAsync(id);
            if (entity == null)
            {
                throw new KeyNotFoundException($"{typeof(TDbEntity)} with id {id} was not found");
            }
            var model = MapEntityToViewModel(entity);
            return model;
        }

        public virtual async Task<TViewModel> CreateAsync(TViewModel model)
        {
            var entity = MapViewModelToEntity(model, null);
            _table.Add(entity);
            await SaveAync();
            var newValue = entity.GetKeyPropertyValue<TColumnDataType>();
            model.SetKeyColumnValue(newValue);
            return model;
        }

        public virtual async Task UpdateAsync(TViewModel model)
        {
            var keyColumnValue = model.GetKeyPropertyValue<object>();
            if (keyColumnValue == null)
            {
                keyColumnValue = model.GetPropertyValue("Id");
            }
            if (keyColumnValue == null)
            {
                throw new KeyNotFoundException("Primary Key column is not defined in the View Model. Please use Id or attribute the [Key]");
            }
            var entity = await _table.FindAsync(keyColumnValue);
            MapViewModelToEntity(model, entity);
            _table.Update(entity);
            await SaveAync();
        }

        public virtual async Task DeleteAsync(TColumnDataType id)
        {
            var entity = await _table.FindAsync(id);
            if (entity == null) { throw new Exception($"There is no entity found with Id{id}"); }
            _table.Remove(entity);
            await SaveAync();
        }

        public virtual TViewModel MapEntityToViewModel(TDbEntity entity, TViewModel model = null, params Expression<Func<TViewModel, object>>[] ignoreProperties)
        {
            model = entity.MapTo(model, ignoreProperties);
            return model;
        }

        public virtual TDbEntity MapViewModelToEntity(TViewModel model, TDbEntity entity, params Expression<Func<TDbEntity, object>>[] ignoreProperties)
        {
            entity = model.MapTo(entity, ignoreProperties);
            entity.MapAuditColumns<TColumnDataType>();
            return entity;
        }
    }
}
