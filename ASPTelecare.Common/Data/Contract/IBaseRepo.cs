﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASPTelecare.Common.Data.Contract
{
    public interface IBaseRepo: IDisposable
    {
        //UserContext CurrentUser { get; set; }
        Task<int> SaveAync();
    }
}
