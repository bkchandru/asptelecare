﻿using ASPTelecare.Common.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ASPTelecare.Common.Data.Contract
{
    public interface ICurdRepo<TDbEntity, TViewModel, TColumnDataType> :IBaseRepo
       where TDbEntity : class
       where TViewModel : class
    {
        #region Read Operations

        Task<IEnumerable<TViewModel>> GetAllAsync();

        Task<PagingList<object>> GetAllPagedAsync(PagingDetails pagingdetails);
        Task<TViewModel> GetByIdAsync(TColumnDataType id);

        #endregion Read Operations

        Task<TViewModel> CreateAsync(TViewModel model);

        Task UpdateAsync(TViewModel model);

        Task DeleteAsync(TColumnDataType id);
    }
}
