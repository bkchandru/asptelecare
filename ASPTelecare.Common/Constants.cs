﻿namespace ASPTelecare.Common
{
    public static class Constants
    {
        public const string AntiforgeryTokenName = "X-TOKEN-ANTIFORGERY";
        public const string AspTelecareServiceName = "ASPTelecare.Service";
        public const string PaymentServiceNameName = "PaymentService";

        public const string KeyColumn = "Id";
        public const string AddedByColumn = "AddedBy";
        public const string AddedDateColumn = "AddedDate";
        public const string UpdatedByColumn = "UpdatedBy";
        public const string UpdatedDateColumn = "UpdatedDate";
    }
}
