﻿namespace ASPTelecare.Common.ViewModel
{
    public class PagingDetails
    {
        public int Skip { get; set; } = 0;
        public int Take { get; set; } = 10;
        public string SortColumn { get; set; } = "Id";
        public bool SortAscending { get; set; } = true;
        public string Filter { get; set; } = string.Empty;
        public int Draw { get; set; } = 1;
    }
}
