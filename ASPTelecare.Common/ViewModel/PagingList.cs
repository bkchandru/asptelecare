using Newtonsoft.Json;
using System.Collections.Generic;

namespace ASPTelecare.Common.ViewModel
{
    public class PagingList<TViewModel>
    {
        [JsonProperty("recordsTotal")]
        public int RecordCount { get; set; }
        [JsonProperty("recordsFiltered")]
        public int FilteredRecordCount { get; set; }
        [JsonProperty("data")]
        public IEnumerable<TViewModel> Data { get; set; }

        [JsonProperty("draw")]
        public int Draw { get; set; }
    }
}
