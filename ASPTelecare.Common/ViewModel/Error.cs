﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ASPTelecare.Common.ViewModel
{
    public class Error
    {
        public string Code { get; set; } = "MISSING_REQUIRED_FIELDS";
        public string Name { get; set; } = "Unprocessable Entity";
        public string Message { get; set; }
        public string Stack { get; set; }
        public string CorrelationId { get; set; }

    }
}
