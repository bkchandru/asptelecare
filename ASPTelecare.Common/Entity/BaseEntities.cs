﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ASPTelecare.Common.Entity
{
    public interface IDbIdColumn<TKeyType>
    {
        TKeyType Id { get; set; }
    }

    public interface IDbAuditColumns
    {
        string UpdatedBy { get; set; }
        DateTime? UpdatedDate { get; set; }
        string AddedBy { get; set; }
        DateTime AddedDate { get; set; }
    }

    public interface IDbDeletedColumn
    {
        bool Deleted { get; set; }
    }

    public interface IDbInstituteColumn
    {
        int InstituteId { get; set; }
    }

    public abstract class BaseDbIdColumn<TKeyType> : IDbIdColumn<TKeyType>
    {
        [Key, Required]
        public TKeyType Id { get; set; }
    }
}
