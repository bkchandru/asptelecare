﻿using ASPTelecare.Common.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ASPTelecare.Common.Business.Contract
{
    public interface ICurdManager<TDbEntity, TViewModel, TColumnDataType> :IBaseManager
       where TDbEntity : class
       where TViewModel : class
    {
        #region Read Operations

        Task<IEnumerable<TViewModel>> GetAllAsync();

        Task<PagingList<object>> GetAllPagedAsync(PagingDetails pagingdetails);
        Task<TViewModel> GetByIdAsync(TColumnDataType id);

        #endregion Read Operations

        Task<TViewModel> CreateAsync(TViewModel viewModel);

        Task UpdateAsync(TViewModel viewModel);

        Task DeleteAsync(TColumnDataType id);
    }
}
