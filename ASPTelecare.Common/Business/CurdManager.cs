﻿using ASPTelecare.Common.Business.Contract;
using ASPTelecare.Common.Data.Contract;
using ASPTelecare.Common.ViewModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ASPTelecare.Common.Business
{
    public abstract class CurdManager<TDbEntity, TViewModel, TColumnDataType> :BaseManager, ICurdManager<TDbEntity, TViewModel, TColumnDataType>
        where TDbEntity : class, new()
        where TViewModel : class, new()
    {
        private readonly ICurdRepo<TDbEntity, TViewModel, TColumnDataType> _repo;

        #region Constructor

        protected CurdManager(ICurdRepo<TDbEntity, TViewModel, TColumnDataType> repo)
            : base(repo)
        {
            _repo = repo;
        }

        #endregion Constructor

        #region Interface Members
        public virtual async Task<TViewModel> CreateAsync(TViewModel viewModel)
        {
            return await _repo.CreateAsync(viewModel);
        }

        public virtual async Task UpdateAsync(TViewModel viewModel)
        {
            await _repo.UpdateAsync(viewModel);
        }

        public virtual async Task DeleteAsync(TColumnDataType id)
        {
            await _repo.DeleteAsync(id);
        }

        public virtual async Task<IEnumerable<TViewModel>> GetAllAsync()
        {
            return await _repo.GetAllAsync();
        }

        public virtual async Task<PagingList<object>> GetAllPagedAsync(PagingDetails pagingdetails)
        {
            return await _repo.GetAllPagedAsync(pagingdetails);
        }

        public virtual async Task<TViewModel> GetByIdAsync(TColumnDataType id)
        {
            return await _repo.GetByIdAsync(id);
        }

        #endregion Interface Members
    }
}
