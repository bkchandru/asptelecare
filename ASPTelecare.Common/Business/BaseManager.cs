﻿using ASPTelecare.Common.Business.Contract;
using ASPTelecare.Common.Data.Contract;

namespace ASPTelecare.Common.Business
{
    public class BaseManager : IBaseManager
    {
        #region Private Variables

        private IBaseRepo _repo;

        #endregion

        #region Constructor

        public BaseManager(IBaseRepo repo)
        {
            _repo = repo;
        }

        #endregion
    }
}
