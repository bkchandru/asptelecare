﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace ASPTelecare.Common.Helper
{
    [HtmlTargetElement("input", Attributes = "asp-for")]
    [HtmlTargetElement("label", Attributes = "asp-for")]
    [HtmlTargetElement("textarea", Attributes = "asp-for")]
    public sealed class HtmlInputTagHelper : TagHelper
    {
        #region Model Expresion

        [HtmlAttributeName("asp-for")]

        public ModelExpression Model { get; set; }

        #endregion

        #region Process Tag Helper Context

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            base.Process(context, output);
            bool required = ValidateMandatory(Model.ModelExplorer.Metadata.ValidatorMetadata);
            SetControlStyle(context, output, required);

            // Dynamically set max length if it is not mentioned in client side 
            if (context.TagName.ToLower() == "input" && context.AllAttributes["maxlength"] == null)
            {
                // Attempt to check for a MaxLength annotation
                var maxLength = GetMaxLength(Model.ModelExplorer.Metadata.ValidatorMetadata);
                if (maxLength > 0)
                {
                    output.Attributes.Add("maxlength", maxLength);
                }
            }
            else if (context.TagName.ToLower() == "textarea" && context.AllAttributes["maxlength"] == null)
            {
                // Attempt to check for a MaxLength annotation
                var maxLength = GetMaxLength(Model.ModelExplorer.Metadata.ValidatorMetadata);
                if (maxLength > 0)
                {
                    output.Attributes.Add("maxlength", maxLength);
                }
            }
        }

        #endregion

        #region Private Methods

        private static bool ValidateMandatory(IReadOnlyList<object> validatorMetadata)
        {
            bool flag = false;
            for (var i = 0; i < validatorMetadata.Count; i++)
            {
                var requiredAttribute = validatorMetadata[i] as RequiredAttribute;
                if (requiredAttribute != null)
                {
                    flag = true;
                }
            }

            return flag;
        }

        private static void SetControlStyle(TagHelperContext context, TagHelperOutput output, bool isRequired)
        {
            string labelStyle = "control-label";
            if (context.TagName.ToLower() == "label")
            {
                labelStyle = isRequired ? "control-label required" : labelStyle;
                output.Attributes.Add("class", labelStyle);
            }

            string inputStyle = "form-control form-control-sm";
            if (context.TagName.ToLower() == "input")
            {
                output.Attributes.Add("autofocus", "");
                inputStyle = isRequired ? "form-control form-control-sm required" : inputStyle;
                output.Attributes.Add("class", inputStyle);
            }

            string textareaStyle = "form-control form-control-sm";
            if (context.TagName.ToLower() == "textarea")
            {
                output.Attributes.Add("autofocus", "");
                textareaStyle = isRequired ? "form-control form-control-sm required" : textareaStyle;
                output.Attributes.Add("class", textareaStyle);
            }
        }

        private static int GetMaxLength(IReadOnlyList<object> validatorMetadata)
        {
            for (var i = 0; i < validatorMetadata.Count; i++)
            {
                var stringLengthAttribute = validatorMetadata[i] as StringLengthAttribute;
                if (stringLengthAttribute != null && stringLengthAttribute.MaximumLength > 0)
                {
                    return stringLengthAttribute.MaximumLength;
                }

                var maxLengthAttribute = validatorMetadata[i] as MaxLengthAttribute;
                if (maxLengthAttribute != null && maxLengthAttribute.Length > 0)
                {
                    return maxLengthAttribute.Length;
                }
            }

            return 0;
        }

        #endregion
    }
}
