﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Redis;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using NLog;
using System;
using System.Collections.Generic;

namespace ASPTelecare.Common.Helper
{
    public static class ConfigurationManager
    {
        public static AppSetting AppSetting { get; set; }
        public static IConfiguration SetConfiguration(this IConfiguration configuration, IHostingEnvironment env, bool isUiApplication = false)
        {
            var builder = new ConfigurationBuilder()
                      .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                      .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
                      .AddEnvironmentVariables();

            configuration = builder.Build();
            return configuration;
        }

        public static void AddCache(this IServiceCollection services)
        {
            if (AppSetting.CacheEnabled)
            {
                services.AddSingleton<IDistributedCache>(serviceProvider =>
                   new RedisCache(new RedisCacheOptions
                   {
                       Configuration = AppSetting.CacheDbConnectionString,
                       InstanceName = AppSetting.CacheInstanceName
                   }));
            }
            else // default in memory Cache
            {
                services.AddDistributedMemoryCache();
            }
        }

        public static void AddProfiler(this IServiceCollection services)
        {
            if (AppSetting.ProfilerEnabled)
            {
               services.AddMiniProfiler(options =>
               {
                   options.RouteBasePath = "/profiler";
                   options.TrackConnectionOpenClose = true;
               });
            }
        }

        public static void UseProfiler(this IApplicationBuilder app, IHostingEnvironment env)
        {
            if (AppSetting.ProfilerEnabled)
            {
                app.UseMiniProfiler();
            }
        }
        public static void AddExternalHttpServiceSettings(this IServiceCollection services)
        {
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            IHttpClientBuilder httpClientBuilder = null;
            if (AppSetting.Services != null)
            {
                foreach (var service in AppSetting.Services)
                {
                    httpClientBuilder = services.AddHttpClient(service.ServiceName, client =>
                    {
                        client.BaseAddress = new Uri(service.ServiceUrl);
                        client.DefaultRequestHeaders.Add("Accept", "application/vnd.github.v3+json");
                    });
                }
            }
        }

        public static void AddLog(this IServiceCollection services)
        {
            LogManager.Configuration.Variables["connectionString"] = AppSetting.LogDbConnectionString;
        }
    }

    public class AppSetting
    {
        public string DbConnectionString { get; set; }
        public string LogDbConnectionString { get; set; }
        public string FileDbConnectionString { get; set; }
        public string CacheDbConnectionString { get; set; }

        public bool ProfilerEnabled { get; set; }
        public string WebHostAddress { get; set; }
        public bool CacheEnabled { get; set; }
        public string CacheInstanceName { get; set; }
        public string CorsPolicyName { get; set; }
        public List<Service> Services { get; set; }
    }

    public class Service
    {
        public string ServiceName { get; set; }

        public string ServiceUrl { get; set; }
    }

    
}
