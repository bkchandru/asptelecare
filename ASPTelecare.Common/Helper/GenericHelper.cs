﻿using ASPTelecare.Common.ViewModel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Linq.Dynamic.Core;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ASPTelecare.Common.Helper
{
    public static class GenericHelper
    {
        #region Reflection
        public static TKeyType GetKeyPropertyValue<TKeyType>(this object entity)
        {
            var keyPropertyInfo = entity.GetType().GetRuntimeProperties().FirstOrDefault(c => c.GetCustomAttributes(typeof(KeyAttribute)).Any());
            if (keyPropertyInfo != null)
            {
                var propertyValue = keyPropertyInfo.GetValue(entity, null);
                return (TKeyType)propertyValue;
            }
            return default(TKeyType);
        }

        public static object GetPropertyValue(this object entity, string columnName)
        {
            var pinfo = entity.GetType().GetProperty(columnName);
            if (pinfo == null) { return null; }
            return pinfo.GetValue(entity, null);
        }

        public static void SetKeyColumnValue(this object entity, object value)
        {
            var keyPropertyInfo = entity.GetType().GetRuntimeProperties().FirstOrDefault(c => c.GetCustomAttributes(typeof(KeyAttribute)).Any());
            if (keyPropertyInfo != null)
            {
                keyPropertyInfo.SetValue(entity, value, null);
            }
        }

        #endregion Reflection

        #region Paging & Sorting

        public static string ToYesNo(this bool property)
        {
            return property ? "Yes" : "No";
        }

        public static bool YesNoToBoolean(this string property)
        {
            return property == "Yes";
        }
        public static IQueryable<TModel> FullTextFilter<TModel>(this IQueryable<TModel> query, string searchText, string[] ignoreProperties = null, bool ignoreAuditColumns = true) where TModel : class
        {
            if (!string.IsNullOrEmpty(searchText))
            {
                searchText = searchText.ToLower();
                if (ignoreProperties == null)
                {
                    ignoreProperties = new string[] { };
                }
                if (ignoreAuditColumns)
                {
                    ignoreProperties = ignoreProperties.Concat(new string[] { Constants.AddedDateColumn, Constants.AddedByColumn, Constants.UpdatedDateColumn, Constants.UpdatedByColumn }).ToArray();
                }
                var properties = query.ElementType.GetProperties();
                var whereClause = new StringBuilder();
                foreach (var property in properties)
                {
                    if (ignoreProperties.Contains(property.Name)) { continue; }
                    if (property.PropertyType == typeof(string))
                    {
                        if (searchText == "Yes" || searchText == "No")
                        {
                            whereClause.Append($"{property.Name}={searchText.YesNoToBoolean()} OR ");
                        }
                        else
                        {
                            whereClause.Append($"{property.Name}.ToLower().Contains(\"{searchText}\") OR ");
                        }

                    }
                    else if (property.PropertyType == typeof(bool))
                    {
                        bool boolean;
                        Boolean.TryParse(searchText, out boolean);
                        if (boolean != default(Boolean))
                        {
                            whereClause.Append($"{property.Name}={Boolean.Parse(searchText)} OR ");
                        }
                    }
                    else if (property.PropertyType == typeof(DateTime))
                    {
                        DateTime datetime;
                        DateTime.TryParse(searchText, out datetime);
                        if (datetime != default(DateTime))
                        {
                            whereClause.Append($"{property.Name}={DateTime.Parse(searchText)} OR ");
                        }
                    }
                    else if (property.PropertyType == typeof(int)
                        || property.PropertyType == typeof(long))
                    {
                        int integer;
                        int.TryParse(searchText, out integer);
                        if (integer > 0)
                        {
                            whereClause.Append($"{property.Name}={searchText} OR ");
                        }
                    }
                    else
                    {
                        Debug.WriteLine($"{property.Name} is ignored from Where Clause");
                    }

                }
                var filter = whereClause.ToString().TrimEnd("OR ".ToCharArray());
                query = query.Where(filter);
            }
            return query;
        }

        public static async Task<PagingList<TViewModel>> ToPagingListAsync<TViewModel>
            (this IQueryable<TViewModel> query, PagingDetails pagingDetails) where TViewModel : class
        {
            query = GetSortQuery(query, pagingDetails);
            var recordsCount = query.Count();
            var pagedResult = new PagingList<TViewModel> { RecordCount = recordsCount, FilteredRecordCount = recordsCount, Draw = pagingDetails.Draw };
            pagedResult.Data = await query.ToListAsync();
            return pagedResult;
        }

        private static IQueryable<TViewModel> GetSortQuery<TViewModel>(IQueryable<TViewModel> query, PagingDetails pagingDetails)
        {
            if (pagingDetails.SortColumn != null && pagingDetails.SortColumn.Length > 0)
            {
                var sortDirection = pagingDetails.SortAscending ? "asc" : "desc";
                var sortExpression = $"{pagingDetails.SortColumn} {sortDirection}";
                query = query.OrderBy(sortExpression);
            }
            return query;
        }

        /// <summary>
        /// converts collection to DataTablesGridPagingInfo.
        /// </summary>
        /// <param name="formCollection"></param>
        /// <returns></returns>
        public static PagingDetails ToGridPagingInfo(this HttpRequest request)
        {
            var pagingInfo = new PagingDetails() { Draw = 1, Skip = 0, Take = int.MaxValue, Filter = "", SortColumn = "Id", SortAscending = true };
            if (!String.IsNullOrEmpty(request.Query["draw"]))
            {
                pagingInfo.Draw = int.Parse(request.Query["draw"]);
            }
            pagingInfo.Skip = int.Parse(request.Query["start"]);
            pagingInfo.Take = int.Parse(request.Query["length"]);
            pagingInfo.SortColumn = request.Query["SortColumn"];
            pagingInfo.SortAscending = request.Query["SortDirection"].ToString().ToUpper() == "ASC";
            pagingInfo.Filter = request.Query["Search_FullText"].ToString().Trim();
            return pagingInfo;
        }

        #endregion Paging & Sorting

        #region Entity Mappers

        public static void MapAuditColumns<TKeyType>(this object auditEntity)
        {
            //Thread.CurrentThread.
            //int userId = FrameworkClaimsPrincipal.UserId;
            //userId = userId == 0 ? Constant.SystemServiceUserId : userId;
            //if (auditEntity != null && auditEntity is BaseAuditEntity<TKeyType>)
            //{
            //    if (Convert.ToDateTime(GenericHelper.GetColumnValue(Constant.AddedDateColumn, auditEntity)) == default(DateTime))
            //    {
            //        GenericHelper.SetColumnValue(Constant.AddedDateColumn, auditEntity, DateTime.UtcNow);
            //        GenericHelper.SetColumnValue(Constant.AddedByColumn, auditEntity, userId);
            //    }
            //    GenericHelper.SetColumnValue(Constant.ModifiedDateColumn, auditEntity, DateTime.UtcNow);
            //    GenericHelper.SetColumnValue(Constant.ModifiedByColumn, auditEntity, userId);
            //}
        }

        public static TTarget MapTo<TTarget>(this object source, TTarget target = null, params Expression<Func<TTarget, object>>[] ignoreProperties) where TTarget : class, new()
        {
            if (source == null) { return null; }
            if (target == null) { target = new TTarget(); }
            var sourceProperties = source.GetType().GetProperties();
            var destinationProperties = typeof(TTarget).GetProperties().Where(dest => dest.CanWrite);
            foreach (var destinationProperty in destinationProperties)
            {
                var sourceProperty = sourceProperties.FirstOrDefault(src => src.Name == destinationProperty.Name);
                if (sourceProperty != null)
                {
                    if (sourceProperty.PropertyType == destinationProperty.PropertyType)
                    {
                        bool canIgnore = false;
                        if (ignoreProperties != null)
                        {
                            foreach (var ignoreProperty in ignoreProperties)
                            {
                                var body = ignoreProperty.Body as MemberExpression;
                                if (body == null)
                                {
                                    body = ((UnaryExpression)ignoreProperty.Body).Operand as MemberExpression;
                                }
                                if (destinationProperty.Name == body.Member.Name)
                                {
                                    canIgnore = true;
                                    break;
                                }
                            }
                        }
                        if (!canIgnore)
                        {
                            destinationProperty.SetValue(target, sourceProperty.GetValue(source));
                        }

                    }
                }
            }
            return target;
        }

        #endregion Entity Mappers

        #region Session Handler

        public static void SetSession<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetSession<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        #endregion Session Handler

        #region Exception Handler
        public static void UseExceptionHandler(this IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseExceptionHandler(
                 options =>
                 {
                     options.Run(
                     async context =>
                     {
                         var status = HttpStatusCode.InternalServerError;
                         context.Response.ContentType = "application/json";

                         var error = new Error()
                         {
                             Name = "",
                             Message = "An unhandled error occured!",
                             Code = status.ToString(),
                             CorrelationId = context.TraceIdentifier
                         };

                         var ex = context.Features.Get<IExceptionHandlerFeature>();
                         if (ex != null)
                         {
                             if (env.IsDevelopment())
                             {
                                 error.Message = ex.Error.Message;
                                 error.Stack = ex.Error.StackTrace;
                             }

                             var errors = new List<Error>();
                             var exceptionType = ex.Error.GetType();

                             if (exceptionType == typeof(UnauthorizedAccessException))
                             {
                                 error.Message = "Unauthorized Access";
                                 status = HttpStatusCode.Unauthorized;
                             }
                             else if (exceptionType == typeof(NotImplementedException))
                             {
                                 error.Message = "The method is not implemented.";
                                 status = HttpStatusCode.NotImplemented;
                             }
                             else if (exceptionType == typeof(ValidationException))
                             {
                                 var validationException = ex.Error as ValidationException;
                                 //error.Name = businessException.Name;
                                 error.Message = validationException.Message;
                                 //error.Code = businessException.Code;
                                 status = HttpStatusCode.BadRequest;
                                 errors.Add(error);
                                 context.Response.StatusCode = (int)status;
                                 await context.Response.WriteAsync(JsonConvert.SerializeObject(errors)).ConfigureAwait(false);
                                 return;
                             }

                             errors.Add(error);
                             context.Response.StatusCode = (int)status;
                             await context.Response.WriteAsync(JsonConvert.SerializeObject(errors)).ConfigureAwait(false);
                         }
                     });
                 }
             );
        }

        public static JsonResult ToJsonError(this ModelStateDictionary modelState)
        {
            return new JsonResult(modelState.GetModelStateError()) { StatusCode = (int)HttpStatusCode.BadRequest };
        }

        public static Error[] GetModelStateError(this ModelStateDictionary modelState)
        {
            var errors = modelState
                        .Where(e => e.Value.Errors.Count > 0)
                        .Select(e => new Error
                        {
                            Code = ((int)HttpStatusCode.BadRequest).ToString(),
                            Name = e.Key,
                            Message = e.Value.Errors.First().ErrorMessage
                        }).ToArray();
            return errors;
        }

        #endregion Exception Handler
    }
}
