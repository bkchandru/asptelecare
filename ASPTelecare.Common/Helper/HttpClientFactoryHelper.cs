﻿using ASPTelecare.Common.ViewModel;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ASPTelecare.Common.Helper
{
    public static class HttpClientFactoryHelper
    {
        public static async Task<HttpResponse<TReturn>> GetAsync<TReturn>(this HttpClient httpClient, string requestUri)
        {
            var response = await httpClient.GetAsync(requestUri);
            return await ProcessHttpResponseAsync<TReturn>(response);
        }

        public static async Task<HttpResponse<TResponse>> PostAsync<TResponse>(this HttpClient httpClient, string requestUri, TResponse model)
        {
            var content = SerializeToJsonContent(model);
            var response = await httpClient.PostAsync(requestUri, content);
            return await ProcessHttpResponseAsync<TResponse>(response);
        }

        public static async Task<HttpResponse<TResponse>> PostAsync<TInput, TResponse>(this HttpClient httpClient, string requestUri, TInput model)
        {
            var content = SerializeToJsonContent(model);
            var response = await httpClient.PostAsync(requestUri, content);
            return await ProcessHttpResponseAsync<TResponse>(response);
        }

        public static async Task<HttpResponse<TResponse>> PostAsync<TResponse>(this HttpClient httpClient, string requestUri, HttpContent content) where TResponse : class
        {
            var response = await httpClient.PostAsync(requestUri, content);
            return await ProcessHttpResponseAsync<TResponse>(response);
        }

        public static async Task<HttpResponse<TResponse>> PutAsync<TResponse, TInput>(this HttpClient httpClient, string requestUri, TInput model) where TResponse : class
        {
            var content = SerializeToJsonContent(model);
            var response = await httpClient.PutAsync(requestUri, content);
            return await ProcessHttpResponseAsync<TResponse>(response);
        }

        /// <summary>
        /// PutAsync Method.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TInput"></typeparam>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="requestUri"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static async Task<HttpResponse<TResponse>> PutAsync<TInput, TResponse>(this HttpClient httpClient, string requestUri, TInput model)
        {
            var content = SerializeToJsonContent(model);
            var response = await httpClient.PutAsync(requestUri, content);
            return await ProcessHttpResponseAsync<TResponse>(response);
        }

        public static async Task<HttpResponse<TInput>> PutAsync<TInput>(this HttpClient httpClient, string requestUri, TInput model)
        {
            var content = SerializeToJsonContent(model);
            var response = await httpClient.PutAsync(requestUri, content);
            return await ProcessHttpResponseAsync<TInput>(response);
        }

        public static async Task<HttpResponse<TResponse>> DeleteAsync<TResponse>(this HttpClient httpClient, string requestUri)
        {
            var response = await httpClient.DeleteAsync(requestUri);
            return await ProcessHttpResponseAsync<TResponse>(response);
        }

        #region Helper Methods

        public static async Task<HttpResponse<T>> ProcessHttpResponseAsync<T>(HttpResponseMessage response)
        {
            HttpResponse<T> result = new HttpResponse<T>();
            var content = await response.Content.ReadAsStringAsync();
            result.StatusCode = response.StatusCode;
            result.StatusMessage = response.ReasonPhrase;
            result.IsSuccessStatusCode = response.IsSuccessStatusCode;
            if (response.IsSuccessStatusCode)
            {
                result.Body = JsonConvert.DeserializeObject<T>(content);
            }
            else
            {
                if (!string.IsNullOrEmpty(content))
                {
                    var error = JsonConvert.DeserializeObject<List<Error>>(content);
                    result.StatusMessage = error.First().Message.ToString().TrimEnd('"').TrimStart('"');
                    result.CorrelationId = error.First().CorrelationId;
                }
            }

            return result;
        }

        public static StringContent SerializeToJsonContent(object model)
        {
            var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            return content;
        }

        #endregion Helper Methods
    }

    public class HttpResponse<TOutput>
    {
        public TOutput Body { get; set; }

        public bool IsSuccessStatusCode { get; set; }

        public HttpStatusCode StatusCode { get; set; }

        public string StatusMessage { get; set; }

        public string CorrelationId { get; set; }

        public string Stack { get; set; }
    }
}
