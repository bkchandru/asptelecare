﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;

namespace ASPTelecare.Contract.Data.Account
{
    public interface IRoleRepo<TRole, TContext, TKey, TUserRole, TRoleClaim> : IQueryableRoleStore<TRole>, IRoleStore<TRole>, IDisposable, IRoleClaimStore<TRole>
        where TRole : IdentityRole<TKey>
        where TContext : DbContext
        where TKey : IEquatable<TKey>
        where TUserRole : IdentityUserRole<TKey>, new()
        where TRoleClaim : IdentityRoleClaim<TKey>, new()
    {
        /// <summary>
        /// Gets or sets the Microsoft.AspNetCore.Identity.IdentityErrorDescriber for any error that occurred with the current operation.
        /// </summary>
        IdentityErrorDescriber ErrorDescriber { get; set; }
         
        /// <summary>
        /// Gets the database context for this store.
        /// </summary>
        TContext Context { get; }
           
        /// <summary>
        /// Gets or sets a flag indicating if changes should be persisted after CreateAsync,UpdateAsync and DeleteAsync are called.
        /// </summary>
        bool AutoSaveChanges { get; set; }
       
        // Summary:
        //     Converts the provided id to a strongly typed key object.
        //
        // Parameters:
        //   id:
        //     The id to convert.
        //
        // Returns:
        //     An instance of TKey representing the provided id.
        TKey ConvertIdFromString(string id);
        //
        // Summary:
        //     Converts the provided id to its string representation.
        //
        // Parameters:
        //   id:
        //     The id to convert.
        //
        // Returns:
        //     An System.String representation of the provided id.
        string ConvertIdToString(TKey id);
        
    }
}
