﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ASPTelecare.Contract.Data.Account
{
    public interface IUserStoreBase<TUser, TRole, TKey, TUserClaim, TUserRole, TUserLogin, TUserToken, TRoleClaim>
    {
        /// <summary>
        /// Adds the given normalizedRoleName to the specified user.
        /// </summary>
        /// <param name="user">The user to add the role to.</param>
        /// <param name="normalizedRoleName">The role to add.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns>The System.Threading.Tasks.Task that represents the asynchronous operation.</returns>
        Task AddToRoleAsync(TUser user, string normalizedRoleName, CancellationToken cancellationToken = default(CancellationToken));
        
        /// <summary>
        /// Retrieves the roles the specified user is a member of.
        /// </summary>
        /// <param name="user">The user whose roles should be retrieved.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns>A System.Threading.Tasks.Task`1 that contains the roles the user is a member of.</returns>
        Task<IList<string>> GetRolesAsync(TUser user, CancellationToken cancellationToken = default(CancellationToken));
        
        /// <summary>
        /// Retrieves all users in the specified role.
        /// </summary>
        /// <param name="normalizedRoleName">The role whose users should be retrieved.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns>The System.Threading.Tasks.Task contains a list of users, if any, that are in the specified role.</returns>
        Task<IList<TUser>> GetUsersInRoleAsync(string normalizedRoleName, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Returns a flag indicating if the specified user is a member of the give normalizedRoleName.
        /// </summary>
        /// <param name="user">The user whose role membership should be checked.</param>
        /// <param name="normalizedRoleName">The role to check membership of</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns>
        /// A System.Threading.Tasks.Task`1 containing a flag indicating if the specified
        /// user is a member of the given group. If the user is a member of the group the
        /// returned value with be true, otherwise it will be false.
        /// </returns>
        Task<bool> IsInRoleAsync(TUser user, string normalizedRoleName, CancellationToken cancellationToken = default(CancellationToken));
        /// <summary>
        /// Removes the given normalizedRoleName from the specified user.
        /// </summary>
        /// <param name="user">The user to remove the role from.</param>
        /// <param name="normalizedRoleName">The role to remove.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns>The System.Threading.Tasks.Task that represents the asynchronous operation.</returns>
        Task RemoveFromRoleAsync(TUser user, string normalizedRoleName, CancellationToken cancellationToken = default(CancellationToken));
    }


    public interface IUserRepo <TUser, TRole, TContext, TKey, TUserClaim, TUserRole, TUserLogin, TUserToken, TRoleClaim> : IUserStoreBase<TUser, TRole, TKey, TUserClaim, TUserRole, TUserLogin, TUserToken, TRoleClaim>, IProtectedUserStore<TUser>, IUserStore<TUser> 
        where TUser : IdentityUser<TKey>
        where TRole : IdentityRole<TKey>
        where TContext : DbContext
        where TKey : IEquatable<TKey>
        where TUserClaim : IdentityUserClaim<TKey>, new()
        where TUserRole : IdentityUserRole<TKey>, new()
        where TUserLogin : IdentityUserLogin<TKey>, new()
        where TUserToken : IdentityUserToken<TKey>, new()
        where TRoleClaim : IdentityRoleClaim<TKey>, new()
    {

        /// <summary>
        /// Gets the database context for this store.
        /// </summary>
        TContext Context { get; }
          
        /// <summary>
        /// Gets or sets a flag indicating if changes should be persisted after CreateAsync, UpdateAsync and DeleteAsync are called.
        /// </summary>
        bool AutoSaveChanges { get; set; }
        
        /// <summary>
        /// A navigation property for the users the store contains.
        /// </summary>
        IQueryable<TUser> Users { get; }

        // Returns:
        //     The System.Threading.Tasks.Task that represents the asynchronous operation.
        /// <summary>
        /// Adds the claims given to the specified user.
        /// </summary>
        /// <param name="user">The user to add the claim to.</param>
        /// <param name="claims">The claim to add to the user.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns></returns>
        Task AddClaimsAsync(TUser user, IEnumerable<Claim> claims, CancellationToken cancellationToken = default(CancellationToken));
        
        /// <summary>
        /// Adds the login given to the specified user.
        /// </summary>
        /// <param name="user">The user to add the login to.</param>
        /// <param name="login">The login to add to the user.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns></returns>
        Task AddLoginAsync(TUser user, UserLoginInfo login, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// 
        /// </summary>
        /// <param name="normalizedEmail"></param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns></returns>
        Task<TUser> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken = default(CancellationToken));
        
        /// <summary>
        /// Retrieves the user associated with the specified login provider and login provider key.
        /// </summary>
        /// <param name="loginProvider">The login provider who provided the providerKey.</param>
        /// <param name="providerKey">The key provided by the loginProvider to identify a user.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns>user, if any which matched the specified login provider and key</returns>
        Task<TUser> FindByLoginAsync(string loginProvider, string providerKey, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Get the claims associated with the specified user as an asynchronous operation.
        /// </summary>
        /// <param name="user">The user whose claims should be retrieved.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns>the claims granted to a user.</returns>
        Task<IList<Claim>> GetClaimsAsync(TUser user, CancellationToken cancellationToken = default(CancellationToken));
        
        /// <summary>
        /// Retrieves the associated logins for the specified .
        /// </summary>
        /// <param name="user">The user whose associated logins to retrieve.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns>List of Microsoft.AspNetCore.Identity.UserLoginInfo for the specified user, if any.</returns>
        Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user, CancellationToken cancellationToken = default(CancellationToken));

        /// <summary>
        /// Retrieves all users with the specified claim.
        /// </summary>
        /// <param name="claim">The claim whose users should be retrieved.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns>The System.Threading.Tasks.Task contains a list of users, if any, that contain the specified claim.</returns>
        Task<IList<TUser>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken = default(CancellationToken));
        
        /// <summary>
        /// Removes the claims given from the specified user.
        /// </summary>
        /// <param name="user">The user to remove the claims from.</param>
        /// <param name="claims">The claim to remove.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns></returns>
        Task RemoveClaimsAsync(TUser user, IEnumerable<Claim> claims, CancellationToken cancellationToken = default(CancellationToken));
        
        /// <summary>
        /// Removes the loginProvider given from the specified user.
        /// </summary>
        /// <param name="user"> The user to remove the login from.</param>
        /// <param name="loginProvider">The login to remove from the user.</param>
        /// <param name="providerKey">The key provided by the loginProvider to identify a user.</param>
        /// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        /// <returns></returns>
        Task RemoveLoginAsync(TUser user, string loginProvider, string providerKey, CancellationToken cancellationToken = default(CancellationToken));
                
        ///// <summary>
        ///// Add a new user token.
        ///// </summary>
        ///// <param name="token">The token to be added.</param>
        ///// <returns></returns>
        //protected override Task AddUserTokenAsync(TUserToken token);
        
        ///// <summary>
        ///// Return a role with the normalized name if it exists.
        ///// </summary>
        ///// <param name="normalizedRoleName">The normalized role name.</param>
        ///// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        ///// <returns>The role if it exists.</returns>
        //protected override Task<TRole> FindRoleAsync(string normalizedRoleName, CancellationToken cancellationToken);
        
        ///// <summary>
        /////  Find a user token if it exists.
        ///// </summary>
        ///// <param name="user">The token owner.</param>
        ///// <param name="loginProvider">The login provider for the token.</param>
        ///// <param name="name">The name of the token.</param>
        ///// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        ///// <returns>The user token if it exists.</returns>
        //protected override Task<TUserToken> FindTokenAsync(TUser user, string loginProvider, string name, CancellationToken cancellationToken);
        
        ///// <summary>
        ///// Return a user with the matching userId if it exists.
        ///// </summary>
        ///// <param name="userId">The user's id.</param>
        ///// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        ///// <returns>The user if it exists.</returns>
        //protected override Task<TUser> FindUserAsync(TKey userId, CancellationToken cancellationToken);
         
        ///// <summary>
        ///// Return a user login with the matching userId, provider, providerKey if it exists.
        ///// </summary>
        ///// <param name="userId">The user's id.</param>
        ///// <param name="loginProvider">The login provider name.</param>
        ///// <param name="providerKey">The key provided by the loginProvider to identify a user.</param>
        ///// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        ///// <returns>The user login if it exists.</returns>
        //protected override Task<TUserLogin> FindUserLoginAsync(TKey userId, string loginProvider, string providerKey, CancellationToken cancellationToken);
        
        ///// <summary>
        ///// Return a user login with provider, providerKey if it exists.
        ///// </summary>
        ///// <param name="loginProvider">The login provider name.</param>
        ///// <param name="providerKey">The key provided by the loginProvider to identify a user.</param>
        ///// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        ///// <returns>The user login if it exists.</returns>
        //protected override Task<TUserLogin> FindUserLoginAsync(string loginProvider, string providerKey, CancellationToken cancellationToken);
        
        ///// <summary>
        ///// Return a user role for the userId and roleId if it exists.
        ///// </summary>
        ///// <param name="userId">The user's id.</param>
        ///// <param name="roleId">The role's id.</param>
        ///// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        ///// <returns>The user role if it exists.</returns>
        //protected override Task<TUserRole> FindUserRoleAsync(TKey userId, TKey roleId, CancellationToken cancellationToken);
       
        ///// <summary>
        ///// Remove a new user token.
        ///// </summary>
        ///// <param name="token">The token to be removed.</param>
        ///// <returns></returns>
        //protected override Task RemoveUserTokenAsync(TUserToken token);
        
        ///// <summary>
        ///// Saves the current store.
        ///// </summary>
        ///// <param name="cancellationToken">The System.Threading.CancellationToken used to propagate notifications that the operation should be canceled.</param>
        ///// <returns>The System.Threading.Tasks.Task that represents the asynchronous operation.</returns>
        //protected Task SaveChanges(CancellationToken cancellationToken);

    }
}
