﻿using ASPTelecare.Common;
using ASPTelecare.Common.Data.Contract;
using ASPTelecare.Entity.Common;
using ASPTelecare.ViewModel.Common;
using ASPTelecare.ViewModel.Patient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASPTelecare.Contract.Data.Common
{
    public interface IListMasterRepo : ICurdRepo<ListMaster, ListMasterViewModel,int>
    {
        Task<List<ListMasterViewModel>> GetByListTypeAsync(ListType listType);
    }
}
