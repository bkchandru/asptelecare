﻿using ASPTelecare.Common.Data.Contract;
using ASPTelecare.ViewModel.Patient;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASPTelecare.Contract.Data.Patient
{
    public interface IPatientRepo : ICurdRepo<Entity.Patient, PatientViewModel, int>
    {
    }
}
