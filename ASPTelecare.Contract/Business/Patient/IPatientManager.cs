﻿using ASPTelecare.Common.Business.Contract;
using ASPTelecare.ViewModel.Patient;
using System;
using System.Collections.Generic;
using System.Text;

namespace ASPTelecare.Contract.Business.Patient
{
    public interface IPatientManager: ICurdManager<Entity.Patient, PatientViewModel,int>
    {
    }
}
