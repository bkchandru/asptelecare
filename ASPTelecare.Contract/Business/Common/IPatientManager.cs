﻿using ASPTelecare.Common;
using ASPTelecare.Common.Business.Contract;
using ASPTelecare.Entity.Common;
using ASPTelecare.ViewModel.Common;
using ASPTelecare.ViewModel.Patient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASPTelecare.Contract.Business.Common
{
    public interface IListMasterManager: ICurdManager<ListMaster, ListMasterViewModel,int>
    {
        Task<List<ListMasterViewModel>> GetByListTypeAsync(ListType listType);
    }
}
