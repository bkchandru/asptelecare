﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ASPTelecare.Contract.Business.Account
{
    public interface IRoleManager<TRole> where TRole : class
    {

        /// <summary>
        /// Gets an IQueryable collection of Roles if the persistence store is an Microsoft.AspNetCore.Identity.IQueryableRoleStore`1, otherwise throws a System.NotSupportedException.
        /// </summary>
        /// <remarks>
        /// Callers to this property should use Microsoft.AspNetCore.Identity.RoleManager`1.SupportsQueryableRoles
        /// to ensure the backing role store supports returning an IQueryable list of roles.
        /// </remarks>
        IQueryable<TRole> Roles { get; }
        
        /// <summary>
        /// Gets the normalizer to use when normalizing role names to keys.
        /// </summary>
        ILookupNormalizer KeyNormalizer { get; set; }
           
        /// <summary>
        /// Gets the Microsoft.AspNetCore.Identity.IdentityErrorDescriber used to provider error messages.
        /// </summary>
        IdentityErrorDescriber ErrorDescriber { get; set; }
           
        /// <summary>
        /// Gets a list of validators for roles to call before persistence.
        /// </summary>
        IList<IRoleValidator<TRole>> RoleValidators { get; }
          
        /// <summary>
        /// Gets the Microsoft.Extensions.Logging.ILogger used to log messages from the manager.
        /// </summary>
        ILogger Logger { get; set; }
          
        /// <summary>
        /// Gets a flag indicating whether the underlying persistence store supports returning an System.Linq.IQueryable collection of roles.
        /// </summary>
        bool SupportsQueryableRoles { get; }
          
        /// <summary>
        /// Gets a flag indicating whether the underlying persistence store supports System.Security.Claims.Claims for roles.
        /// </summary>
        bool SupportsRoleClaims { get; }

        /// <summary>
        /// Adds a claim to a role.
        /// </summary>
        /// <param name="role">The role to add the claim to.</param>
        /// <param name="claim">The claim to add.</param>
        /// <returns>the Microsoft.AspNetCore.Identity.IdentityResult of the operation.</returns>
        Task<IdentityResult> AddClaimAsync(TRole role, Claim claim);
        
        /// <summary>
        /// Creates the specified role in the persistence store.
        /// </summary>
        /// <param name="role">The role to create.</param>
        /// <returns>returns Identity Result</returns>
        Task<IdentityResult> CreateAsync(TRole role);
        
        /// <summary>
        /// Deletes the specified role.
        /// </summary>
        /// <param name="role">The role to delete.</param>
        /// <returns>the Microsoft.AspNetCore.Identity.IdentityResult for the delete.</returns>
        Task<IdentityResult> DeleteAsync(TRole role);
       
        /// <summary>
        /// Releases all resources used by the role manager.
        /// </summary>
        void Dispose();
       
        /// <summary>
        /// Finds the role associated with the specified roleId if any.
        /// </summary>
        /// <param name="roleId">The role ID whose role should be returned.</param>
        /// <returns>the role associated with the specified roleId</returns>
        Task<TRole> FindByIdAsync(string roleId);
        
        /// <summary>
        /// Finds the role associated with the specified roleName if any.
        /// </summary>
        /// <param name="roleName">The name of the role to be returned.</param>
        /// <returns>the role associated with the specified roleName</returns>
        Task<TRole> FindByNameAsync(string roleName);
        
        /// <summary>
        /// Gets a list of claims associated with the specified role.
        /// </summary>
        /// <param name="role">The role whose claims should be returned.</param>
        /// <returns>the list of System.Security.Claims.Claims associated with the specified role.</returns>
        Task<IList<Claim>> GetClaimsAsync(TRole role);
        
        /// <summary>
        /// Gets the ID of the specified role.
        /// </summary>
        /// <param name="role">The role whose ID should be retrieved.</param>
        /// <returns>the ID of the specified role.</returns>
        Task<string> GetRoleIdAsync(TRole role);
        
        /// <summary>
        /// Gets the name of the specified role.
        /// </summary>
        /// <param name="role">The role whose name should be retrieved.</param>
        /// <returns>the name of the specified role.</returns>
        Task<string> GetRoleNameAsync(TRole role);
        
        /// <summary>
        /// Gets a normalized representation of the specified key.
        /// </summary>
        /// <param name="key">The value to normalize.</param>
        /// <returns>A normalized representation of the specified key.</returns>
        string NormalizeKey(string key);
        
        /// <summary>
        /// Removes a claim from a role.
        /// </summary>
        /// <param name="role">The role to remove the claim from.</param>
        /// <param name="claim">The claim to remove.</param>
        /// <returns>the Microsoft.AspNetCore.Identity.IdentityResult of the operation.</returns>
        Task<IdentityResult> RemoveClaimAsync(TRole role, Claim claim);
        
        /// <summary>
        /// Gets a flag indicating whether the specified roleName exists.
        /// </summary>
        /// <param name="roleName">The role name whose existence should be checked.</param>
        /// <returns>true if the role name exists, otherwise false.</returns>
        Task<bool> RoleExistsAsync(string roleName);
        
        /// <summary>
        /// Sets the name of the specified role.
        /// </summary>
        /// <param name="role">The role whose name should be set.</param>
        /// <param name="name">The name to set.</param>
        /// <returns>the Microsoft.AspNetCore.Identity.IdentityResult of the operation.</returns>
        Task<IdentityResult> SetRoleNameAsync(TRole role, string name);
        
        /// <summary>
        /// Updates the specified role.
        /// </summary>
        /// <param name="role">The role to updated.</param>
        /// <returns>the Microsoft.AspNetCore.Identity.IdentityResult for the update.</returns>
        Task<IdentityResult> UpdateAsync(TRole role);
        
        /// <summary>
        /// Updates the normalized name for the specified role.
        /// </summary>
        /// <param name="role">The role whose normalized name needs to be updated.</param>
        /// <returns>The System.Threading.Tasks.Task that represents the asynchronous operation.</returns>
        Task UpdateNormalizedRoleNameAsync(TRole role);

        ///// <summary>
        ///// Gets the persistence store this instance operates over.
        ///// </summary>
        //protected IRoleStore<TRole> Store { get; }

        ///// <summary>
        ///// The cancellation token used to cancel operations.
        ///// </summary>
        //protected virtual CancellationToken CancellationToken { get; }

        ///// <summary>
        ///// Releases the unmanaged resources used by the role manager and optionally releases the managed resources.
        ///// </summary>
        ///// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        //protected virtual void Dispose(bool disposing);
          
        ///// <summary>
        ///// Throws if this class has been disposed.
        ///// </summary>
        //protected void ThrowIfDisposed();
        
        ///// <summary>
        ///// Called to update the role after validating and updating the normalized role name.
        ///// </summary>
        ///// <param name="role">The role.</param>
        ///// <returns>Whether the operation was successful.</returns>
        //protected virtual Task<IdentityResult> UpdateRoleAsync(TRole role);
        
        ///// <summary>
        ///// Should return Microsoft.AspNetCore.Identity.IdentityResult.Success if validation is successful. This is called before saving the role via Create or Update.
        ///// </summary>
        ///// <param name="role">The role</param>
        ///// <returns>A Microsoft.AspNetCore.Identity.IdentityResult representing whether validation was successful.</returns>
        //protected virtual Task<IdentityResult> ValidateRoleAsync(TRole role);
    }
}
