﻿using ASPTelecare.Business.Account;
using ASPTelecare.Business.Patient;
using ASPTelecare.Common.Helper;
using ASPTelecare.Contract.Business.Account;
using ASPTelecare.Contract.Business.Common;
using ASPTelecare.Contract.Business.Patient;
using ASPTelecare.Contract.Data.Account;
using ASPTelecare.Contract.Data.Common;
using ASPTelecare.Contract.Data.Patient;
using ASPTelecare.Data;
using ASPTelecare.Data.Account;
using ASPTelecare.Data.Patient;
using ASPTelecare.Entity.Account;
using CorrelationId;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Newtonsoft.Json.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.IO;
using System.Reflection;

namespace ASPTelecare.Service
{
    public class Startup
    {
        public Startup(IConfiguration configuration,IHostingEnvironment env)
        {
            Configuration = configuration.SetConfiguration(env, true);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            ConfigurationManager.AppSetting = Configuration.Get<AppSetting>();
            services.AddLog();
            services.AddCorrelationId();
            services.AddExternalHttpServiceSettings();
            services.AddDataProtection().SetDefaultKeyLifetime(TimeSpan.MaxValue);
            services.AddCors(options => options.AddPolicy("AllowAll", p => p.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()));
            services.AddDbContext<AppDbContext>(options =>options.UseLazyLoadingProxies());
            AddSwaggerRegistrations(services);

            #region MVC Core Registration

            var mvcBuilder = services.AddMvcCore(options =>
            {
                options.CacheProfiles.Add("Default", new CacheProfile() { Duration = 60 });
                options.CacheProfiles.Add("Never", new CacheProfile() { Location = ResponseCacheLocation.None, NoStore = true });
            });
            mvcBuilder.AddJsonFormatters();
            mvcBuilder.AddJsonOptions(opt =>
            {
                opt.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                var resolver = opt.SerializerSettings.ContractResolver;
                if (resolver != null)
                {
                    var res = resolver as DefaultContractResolver;
                    res.NamingStrategy = null;  // <<!-- this removes the camelcasing
                }
            });

            mvcBuilder.AddApiExplorer();
            //mvcBuilder.AddAuthorization();
            mvcBuilder.SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            #endregion MVC Core Registration
            
            #region Configure Api BehaviorOptions

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = actionContext =>
                {
                    return new BadRequestObjectResult(actionContext.ModelState.GetModelStateError());
                };
            });

            #endregion Configure Api BehaviorOptions

            services.AddCache();
            
            AddIdentityServices(services);
            AddAppServiceRegistrations(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCorrelationId();
            app.UseExceptionHandler(env);
            if (!env.IsDevelopment())
            {
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCors(ConfigurationManager.AppSetting.CorsPolicyName);
            UserSwagger(app);
            //app.UseAuthentication();
            app.UseMvc();
        }

        private void AddIdentityServices(IServiceCollection services)
        {
            services.AddDbContext<AccountDbContext>(options =>
                options.UseLazyLoadingProxies());

            services.AddIdentity<User, Role>()
                   .AddSignInManager<SignInManager>()
                .AddUserManager<UserManager>()
                .AddUserStore<UserRepo>()
                .AddRoleManager<RoleManager>()
                .AddRoleStore<RoleRepo>()
                   .AddDefaultTokenProviders();

            services.AddScoped<IUserManager<User>, UserManager>();
            services.AddScoped<IUserRepo<User, Role, AccountDbContext, int, UserClaim, UserRole, UserLogin, UserToken, RoleClaim>, UserRepo>();
            services.AddScoped<IRoleManager<Role>, RoleManager>();
            services.AddScoped<IRoleRepo<Role, AccountDbContext, int, UserRole, RoleClaim>, RoleRepo>();
            services.AddScoped<ISignInManager<User>, SignInManager>();

            //services.AddIdentity<ApplicationUser, IdentityRole>(config =>
            //{
            //    config.User.RequireUniqueEmail = true;
            //    config.Password.RequireDigit = true;
            //    config.Password.RequireLowercase = true;
            //    config.Password.RequireUppercase = true;
            //    config.Password.RequireNonAlphanumeric = false;
            //    config.Password.RequiredLength = 8;
            //    config.Cookies.ApplicationCookie.LoginPath = "/Auth/Login";
            //    config.Cookies.ApplicationCookie.ExpireTimeSpan = new TimeSpan(7, 0, 0, 0); // Cookies last 7 days
            //})
            //.AddEntityFrameworkStores<ProjectContext>();
            //services.AddScoped < IUserClaimsPrincipalFactory<ApplicationUser>, AppClaimsPrincipalFactory
        }

        private void AddSwaggerRegistrations(IServiceCollection services)
        {
            #region Swagger Integration

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Asp Telecare",
                    Description = "API Services for ASP Telecare",
                });
                options.IgnoreObsoleteActions();
                options.DescribeAllEnumsAsStrings();
                options.CustomSchemaIds(x => x.FullName);
                var xmlFile = $"{Assembly.GetEntryAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                if (File.Exists(xmlPath))
                {
                    options.IncludeXmlComments(xmlPath);
                }
            });

            #endregion Swagger Integration
        }

        private void UserSwagger(IApplicationBuilder app)
        {
            #region Swagger Integration

            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swagger, httpReq) => swagger.Host = httpReq.Host.Value);
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API Version 1");
                c.RoutePrefix = "";
            });

            #endregion Swagger Integration
        }
        private void AddAppServiceRegistrations(IServiceCollection services)
        {
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddTransient<IPatientManager, PatientManager>();
            services.AddTransient<IPatientRepo, PatientRepo>();
            services.AddTransient<IListMasterManager, ListMasterManager>();
            services.AddTransient<IListMasterRepo, ListMasterRepo>();
        }
    }
}
