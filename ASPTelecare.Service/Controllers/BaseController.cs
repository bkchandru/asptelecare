﻿using Microsoft.AspNetCore.Mvc;

namespace ASPTelecare.Service.Controllers
{
    //[Authorize]
    [ApiController, Route("api/[controller]")]
    public class BaseController: ControllerBase
    {
    }
}
