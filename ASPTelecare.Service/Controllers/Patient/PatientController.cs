﻿using ASPTelecare.Common.ViewModel;
using ASPTelecare.Contract.Business.Patient;
using ASPTelecare.ViewModel.Patient;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ASPTelecare.Service.Controllers
{
    /// <summary>
    /// Patient Controller used to handle patient related operations.
    /// </summary>
    public class PatientController : BaseController
    {
        private readonly IPatientManager _manager;

        /// <summary>
        /// Constructor for <see cref="PatientController"/>
        /// </summary>
        /// <param name="manager"><see cref="IPatientManager"/></param>
        public PatientController(IPatientManager manager)
        {
            _manager = manager;
        }

        /// <summary>
        ///  Gets all the Patients
        /// </summary>
        /// <response code="200">Success</response>
        /// <returns>List of <see cref="PagedList<object>"/>
        [HttpPost("Paged"), ProducesResponseType(200)]
        public async Task<ActionResult<PagingList<object>>> GetAsync([FromBody] PagingDetails pagingDetails)
        {
            return await _manager.GetAllPagedAsync(pagingDetails);
        }

        /// <summary>
        /// Gets the Patient by Identifier
        /// </summary>
        /// <param name="id">The Identifier</param>
        /// <returns>Returns <see cref="PatientViewModel"/> based on the identifier</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response>
        [HttpGet("{id}"), ProducesResponseType(200), ProducesResponseType(404)]
        public async Task<ActionResult<PatientViewModel>> GetAsync(int id)
        {
            return await _manager.GetByIdAsync(id);
        }

        /// <summary>
        /// Creates the new Patient
        /// </summary>
        /// <param name="model"><see cref="PatientViewModel"/></param>
        /// <returns>A newly created <see cref="PatientViewModel"/></returns>
        /// <response code="201">Returns the newly created item</response>
        /// <response code="400">If the item is null</response>
        [HttpPost, ProducesResponseType(201), ProducesResponseType(400)]
        public async Task<ActionResult<PatientViewModel>> PostAsync(PatientViewModel model)
        {
            var result = await _manager.CreateAsync(model);
            return CreatedAtAction(nameof(PostAsync), new { id = result.Id }, result);
        }

        /// <summary>
        /// Updates the Patient
        /// </summary>
        /// <param name="id">The Identifier</param>
        /// <param name="model"><see cref="PatientViewModel"/></param>
        /// <response code="204">No Response returned</response>
        [HttpPut("{id}"), ProducesResponseType(204)]
        public async Task PutAsync(int id, PatientViewModel model)
        {
            await _manager.UpdateAsync(model);
        }

        /// <summary>
        /// Deletes a specific Patient
        /// </summary>
        /// <param name="id">The Identifier</param>
        /// <response code="204">No Response returned</response>
        [HttpDelete("{id}"), ProducesResponseType(204)]
        public async Task DeleteAsync(int id)
        {
            await _manager.DeleteAsync(id);
        }
    }
}
