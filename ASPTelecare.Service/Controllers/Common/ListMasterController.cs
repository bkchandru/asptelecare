﻿using ASPTelecare.Common;
using ASPTelecare.Common.ViewModel;
using ASPTelecare.Contract.Business.Common;
using ASPTelecare.ViewModel.Common;
using ASPTelecare.ViewModel.Patient;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ASPTelecare.Service.Controllers
{
    /// <summary>
    /// List Master Controller used to handle Master list related operations.
    /// </summary>
    public class ListMasterController : BaseController
    {
        private readonly IListMasterManager _manager;

        /// <summary>
        /// Constructor for <see cref="ListMasterController"/>
        /// </summary>
        /// <param name="manager"><see cref="IListMasterManager"/></param>
        public ListMasterController(IListMasterManager manager)
        {
            _manager = manager;
        }

        /// <summary>
        ///  Gets all the List Masters
        /// </summary>
        /// <response code="200">Success</response>
        /// <returns>List of <see cref="PagedList<object>"/>
        [HttpPost("Paged"), ProducesResponseType(200)]
        public async Task<ActionResult<PagingList<object>>> GetAsync([FromBody] PagingDetails pagingDetails)
        {
            return await _manager.GetAllPagedAsync(pagingDetails);
        }

        /// <summary>
        /// Gets the List Master by Identifier
        /// </summary>
        /// <param name="id">The Identifier</param>
        /// <returns>Returns <see cref="ListMasterViewModel"/> based on the identifier</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response>
        [HttpGet("{id}"), ProducesResponseType(200), ProducesResponseType(404)]
        public async Task<ActionResult<ListMasterViewModel>> GetAsync(int id)
        {
            return await _manager.GetByIdAsync(id);
        }

        /// <summary>
        /// Gets the List Master by List Type
        /// </summary>
        /// <param name="lisType">The List Type</param>
        /// <returns>Returns <see cref="ListMasterViewModel"/> based on the identifier</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response>
        [HttpGet("ByType/{lisType}"), ProducesResponseType(200), ProducesResponseType(404)]
        public async Task<ActionResult<List<ListMasterViewModel>>> GetByListTypeAsync(ListType lisType)
        {
            return await _manager.GetByListTypeAsync(lisType);
        }

        /// <summary>
        /// Creates the new List Master
        /// </summary>
        /// <param name="model"><see cref="ListMasterViewModel"/></param>
        /// <returns>A newly created <see cref="PatientViewModel"/></returns>
        /// <response code="201">Returns the newly created item</response>
        /// <response code="400">If the item is null</response>
        [HttpPost, ProducesResponseType(201), ProducesResponseType(400)]
        public async Task<ActionResult<ListMasterViewModel>> PostAsync(ListMasterViewModel model)
        {
            var result = await _manager.CreateAsync(model);
            return CreatedAtAction(nameof(PostAsync), new { id = result.Id }, result);
        }

        /// <summary>
        /// Updates the List Master
        /// </summary>
        /// <param name="id">The Identifier</param>
        /// <param name="model"><see cref="ListMasterViewModel"/></param>
        /// <response code="204">No Response returned</response>
        [HttpPut("{id}"), ProducesResponseType(204)]
        public async Task PutAsync(int id, ListMasterViewModel model)
        {
            await _manager.UpdateAsync(model);
        }

        /// <summary>
        /// Deletes a specific List Master
        /// </summary>
        /// <param name="id">The Identifier</param>
        /// <response code="204">No Response returned</response>
        [HttpDelete("{id}"), ProducesResponseType(204)]
        public async Task DeleteAsync(int id)
        {
            await _manager.DeleteAsync(id);
        }
    }
}
