﻿using ASPTelecare.Common.Helper;
using ASPTelecare.Contract.Business.Account;
using ASPTelecare.Entity.Account;
using ASPTelecare.ViewModel.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ASPTelecare.Service.Controllers.Account
{
    public class UserController : BaseController
    {

        private readonly IUserManager<User> _manager;

        /// <summary>
        /// Constructor for <see cref="SignInController"/>
        /// </summary>
        /// <param name="manager"><see cref="IUserManager"/></param>
        public UserController(IUserManager<User> manager)
        {
            _manager = manager;
        }

        /// <summary>
        /// Creates the new User
        /// </summary>
        /// <param name="model"><see cref="RegisterViewModel"/></param>
        /// <returns>A newly created <see cref="RegisterViewModel"/></returns>
        /// <response code="201">Returns the newly created item</response>
        /// <response code="400">If the item is null</response>
        [HttpPost, ProducesResponseType(201), ProducesResponseType(400)]
        public async Task<ActionResult<IdentityResult>> PostAsync([FromBody]RegisterViewModel model)
        {
            var user = model.MapTo<User>();
            return await _manager.CreateAsync(user, model.Password);
        }
    }
}