﻿using ASPTelecare.Contract.Business.Account;
using ASPTelecare.Entity.Account;
using ASPTelecare.ViewModel.Account;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ASPTelecare.Service.Controllers.Account
{
    /// <summary>
    /// <see cref="SignInController"/> used to sign into the application
    /// </summary>
    public class SignInController : BaseController
    {

        private readonly ISignInManager<User> _manager;

        /// <summary>
        /// Constructor for <see cref="SignInController"/>
        /// </summary>
        /// <param name="manager">The Sign In Manager</param>
        public SignInController(ISignInManager<User> manager)
        {
            _manager = manager;
        }

        /// <summary>
        ///  SignIn using Username and Password
        /// </summary>
        /// <response code="200">Success</response>
        /// <returns><see cref="SignInResult"/>
        [AllowAnonymous,HttpPost("PasswordSignIn"), ProducesResponseType(200)]
        public async Task<ActionResult<Microsoft.AspNetCore.Identity.SignInResult>> PasswordSignInAsync([FromBody]LoginViewModel loginModel)
        {
            var result = await _manager.PasswordSignInAsync(loginModel.UserName, loginModel.Password, loginModel.IsPersistent, false);
            return result;
           
        }

    }
}