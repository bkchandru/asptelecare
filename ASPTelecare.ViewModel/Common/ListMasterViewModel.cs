﻿using ASPTelecare.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ASPTelecare.ViewModel.Common
{
    public class ListMasterViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(20)]
        public string ListType { get; set; }
        [Required, MaxLength(120)]
        public string Value { get; set; }
        [Required, MaxLength(120)]
        public string Text { get; set; }
    }
}
