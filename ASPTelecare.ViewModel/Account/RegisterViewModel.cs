﻿using ASPTelecare.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ASPTelecare.ViewModel.Account
{
    public class RegisterViewModel
    {
        [Required, MaxLength(60),Display(Name ="First Name")]
        public string FirstName { get; set; }

        [Required, MaxLength(60), Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required, EmailAddress, Display(Name = "Email Address")]
        public string UserName { get; set; }

        [Required, DataType(DataType.Password), Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password), Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Date Of Birth")]
        public DateTime? DateOfBirth { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "User Type")]
        public UserType UserType { get; set; } = UserType.Patient;
    }
}
