﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ASPTelecare.ViewModel.Account
{
    public class LoginViewModel
    {
        [Required, EmailAddress,Display(Name ="Email Address")]
        public string UserName { get; set; }

        [Required, DataType(DataType.Password), Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool IsPersistent { get; set; }
        public string ReturnUrl { get; set; }
    }
}
