﻿using ASPTelecare.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace ASPTelecare.ViewModel.User
{
    public class UserViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(60)]
        public string FirstName { get; set; }

        [Required, MaxLength(60)]
        public string LastName { get; set; }
       
        public DateTime DateOfBirth { get; set; }
        
        [Required, MaxLength(255)]
        public string EmailId { get; set; }
        public UserType UserType { get; set; }
    }

    public class UserGridViewModel
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string EmailId { get; set; }
        public string UserType { get; set; }
    }

}
