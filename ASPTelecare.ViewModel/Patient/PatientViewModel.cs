﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.ComponentModel.DataAnnotations;

namespace ASPTelecare.ViewModel.Patient
{
    public class PatientViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(60),Display(Name ="First Name")]
        public string FirstName { get; set; }
        [MaxLength(60), Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Required, MaxLength(60), Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required, MaxLength(20), Display(Name = "Gender")]
        public string Gender { get; set; }
        public SelectList Genders { get; set; }

        [Required, Display(Name = "Date of Birth")]
        public DateTime? DateOfBirth { get; set; } = DateTime.Now;
        [MaxLength(20), Display(Name = "Blood Group")]
        public string BloodGroup { get; set; }

        public SelectList BloodGroups { get; set; }

        [MaxLength(20), Display(Name = "Education")]
        public string EducationLevel { get; set; }
        public SelectList EducationLevels { get; set; }

        [MaxLength(60), Display(Name = "MRN")]
        public string MRN { get; set; }
        public long? FileId { get; set; }

        [MaxLength(20), Display(Name = "Smoking Status")]
        public string SmokingStatus { get; set; }
        public SelectList SmokingStatuses { get; set; }


        [Required, MaxLength(15), Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }
        [MaxLength(15), Display(Name = "Home Number")]
        public string HomeNumber { get; set; }

        [Required, MaxLength(255), Display(Name = "Email Address")]
        public string EmailId { get; set; }
        public int? UserId { get; set; }
    }

    public class PatientGridViewModel
    {
        [Key]
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string BloodGroup { get; set; }
        public string MobileNumber { get; set; }
        public string EmailId { get; set; }
    }

}
