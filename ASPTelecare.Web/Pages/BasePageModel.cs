﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ASPTelecare.Web.Pages
{
    //[Authorize, ResponseCache(CacheProfileName = "Never")]
    [ResponseCache(CacheProfileName = "Never")]
    public class BasePageModel: PageModel
    {
        [TempData]
        public string PageTitle { get; set; }
    }
}
