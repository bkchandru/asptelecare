﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ASPTelecare.Common;
using ASPTelecare.Common.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using ASPTelecare.Common.Helper;

namespace ASPTelecare.Web.Pages.User
{
    public class IndexModel : BasePageModel
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<IndexModel> _logger;
        public IndexModel(IHttpClientFactory factory, ILogger<IndexModel> logger)
        {
            _httpClient = factory.CreateClient(Constants.AspTelecareServiceName);
            _logger = logger;
        }
        
        /// <summary>
        /// Shows User list view.
        /// </summary>
        /// <returns>Returns details for User list view</returns>
        public async Task OnGetAsync()
        {
            this.PageTitle = await Task.FromResult("Users");
        }

        ///// <summary>
        ///// Gets the User details by specified filter condition.
        ///// </summary>
        ///// <returns>Returns the list Users</returns>
        //public async Task<IActionResult> OnGetSearchAsync()
        //{
        //    var result = await _httpClient.PostAsync<PagingDetails, PagingList<UsersViewModel>>("User/Paged", HttpContext.Request.ToDataTablePagingInfo());
        //    return new JsonResult(result.Body);
        //}

        ///// <summary>
        ///// Deletes the selected User.
        ///// </summary>
        ///// <param name="id">User Id</param>
        ///// <returns>Returns view with records </returns>
        //public async Task OnPostDeleteAsync(int id)
        //{
        //    var ids = new List<int>() { id }.ToArray();
        //    await _manager.DeleteAsync(ids);
        //}
    }
}