﻿using ASPTelecare.Common.Helper;
using ASPTelecare.ViewModel.Account;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
//using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ASPTelecare.Web.Pages.Account
{
    public class RegisterModel : BasePageModel
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<RegisterModel> _logger;

        [BindProperty]
        public RegisterViewModel RegisterView { get; set; }
        [TempData]
        public string ErrorMessage { get; set; }
        public RegisterModel(IHttpClientFactory httpClientFactory, ILogger<RegisterModel> logger)
        {
            _httpClient = httpClientFactory.CreateClient("ASPTelecare.Service");
            _logger = logger;
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            PageTitle = "Register New Patient";
            RegisterView = new RegisterViewModel() { ReturnUrl = returnUrl ?? Url.Content("~/") };
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null )
        {
            //RegisterView.ReturnUrl = returnUrl ?? Url.Content("~/Patient/Dashboard");
            RegisterView.ReturnUrl = Url.Content("~/Patient/Dashboard");
            if (ModelState.IsValid)
            {
                var user = new IdentityUser { UserName = RegisterView.UserName, Email = RegisterView.UserName };
                var result = await _httpClient.PostAsync<RegisterViewModel, IdentityResult>("User", RegisterView);
                if (result.IsSuccessStatusCode)
                {
                    var identityResult = result.Body;
                    if (!identityResult.Errors.Any())
                    {
                        _logger.LogInformation("User created a new account with password.");
                        var claims = new List<Claim>();
                        claims.Add(new Claim(ClaimTypes.Name, RegisterView.UserName));
                        var userIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                        var principal = new ClaimsPrincipal(userIdentity);
                        await HttpContext.SignInAsync(principal);
                        return LocalRedirect(returnUrl);
                    }
                    foreach (var error in result.Body.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}