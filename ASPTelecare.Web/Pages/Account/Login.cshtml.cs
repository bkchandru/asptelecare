using ASPTelecare.Common;
using ASPTelecare.Common.Helper;
using ASPTelecare.ViewModel.Account;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace ASPTelecare.Web.Pages.Account
{
    [AllowAnonymous]
    public class LoginModel : BasePageModel
    {
        [BindProperty]
        public LoginViewModel LoginView { get; set; }

        private readonly HttpClient _httpClient;
        private readonly ILogger<LoginModel> _logger;
        public LoginModel(IHttpClientFactory factory, ILogger<LoginModel> logger)
        {
            _httpClient = factory.CreateClient(Constants.AspTelecareServiceName);
            _logger = logger;
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            PageTitle = await Task.FromResult("Login");
            LoginView = new LoginViewModel() { ReturnUrl = returnUrl ?? Url.Content("~/") };
        }

        public async Task<ActionResult> OnPostAsync(string returnUrl = null)
        {
            LoginView.ReturnUrl = returnUrl ?? Url.Content("~/Patient/Index");
            if (ModelState.IsValid)
            {
                try
                {
                    var result = await _httpClient.PostAsync<LoginViewModel, SignInResult>("SignIn/PasswordSignIn", LoginView);
                    if (result.IsSuccessStatusCode)
                    {
                        var signInResult = result.Body;
                        //if (signInResult.Succeeded)
                        //{
                            _logger.LogInformation("User logged in.");
                            await SignInAsync();
                            return LocalRedirect(LoginView.ReturnUrl);
                        //}
                        if (signInResult.RequiresTwoFactor)
                        {
                            return RedirectToPage("./LoginWith2fa", new { ReturnUrl = LoginView.ReturnUrl, RememberMe = LoginView.IsPersistent });
                        }
                        if (signInResult.IsLockedOut)
                        {
                            _logger.LogWarning("User account locked out.");
                            return RedirectToPage("./Lockout");
                        }
                        else
                        {
                            ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                }
            }
            return Page();
        }

        public async Task<IActionResult> OnPostLogoutAsync()
        {
            await HttpContext.SignOutAsync();
            foreach (var cookie in Request.Cookies.Keys)
            {
                Response.Cookies.Delete(cookie);
            }
            return LocalRedirect(Url.Content("~/Account/Login"));
        }

        public async Task SignInAsync()
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, LoginView.UserName));
            claims.Add(new Claim(ClaimTypes.GivenName, "Chandru"));
            claims.Add(new Claim(ClaimTypes.Role, "Patient"));
            var userIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(userIdentity);
            await HttpContext.SignInAsync(principal);
        }
    }
}