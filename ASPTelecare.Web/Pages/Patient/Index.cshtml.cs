using ASPTelecare.Common;
using ASPTelecare.Common.Helper;
using ASPTelecare.Common.ViewModel;
using ASPTelecare.ViewModel.Patient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;

namespace ASPTelecare.Web.Pages.Patient
{
    public class IndexModel : BasePageModel
    {
        private readonly HttpClient _httpClient;
        private readonly ILogger<IndexModel> _logger;
        private readonly IStringLocalizer<IndexModel> _localizer;
        private readonly IDistributedCache _distributedCache;
        public IndexModel(IHttpClientFactory factory, ILogger<IndexModel> logger,
            IStringLocalizer<IndexModel> localizer, IDistributedCache distributedCache)
        {
            _httpClient = factory.CreateClient(Constants.AspTelecareServiceName);
            _logger = logger;
            _localizer = localizer;
            _distributedCache = distributedCache;
        }

        /// <summary>
        /// Shows Patient list view.
        /// </summary>
        /// <returns>Returns details for Patient list view</returns>
        public async Task OnGetAsync()
        {
            this.PageTitle = await Task.FromResult(_localizer["Patients"]);
        }

        /// <summary>
        /// Gets the Patient details by specified filter condition.
        /// </summary>
        /// <returns>Returns the list Patients</returns>
        public async Task<IActionResult> OnGetSearchAsync()
        {
            var result = await _httpClient.PostAsync<PagingDetails, PagingList<PatientViewModel>>("Patient/Paged", HttpContext.Request.ToGridPagingInfo());
            return new JsonResult(result.Body);
        }

        /// <summary>
        /// Deletes the selected Patient.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns>Returns view with records </returns>
        public async Task OnPostDeleteAsync(int id)
        {
            await _httpClient.DeleteAsync<object>($"Patient/{id}");
        }

    }
}