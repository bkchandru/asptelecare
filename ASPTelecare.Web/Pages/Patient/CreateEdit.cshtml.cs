using ASPTelecare.Common;
using ASPTelecare.Common.Helper;
using ASPTelecare.ViewModel.Common;
using ASPTelecare.ViewModel.Patient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ASPTelecare.Web.Pages.Patient
{
    public class CreateEditModel : BasePageModel
    {
        [BindProperty]
        public PatientViewModel PatientView { get; set; }

        private readonly HttpClient _httpClient;
        private readonly ILogger<CreateEditModel> _logger;
        public CreateEditModel(IHttpClientFactory factory, ILogger<CreateEditModel> logger)
        {
            _httpClient = factory.CreateClient(Constants.AspTelecareServiceName);
            _logger = logger;
        }

        public async Task OnGetAsync(int? id)
        {
            PatientView = new PatientViewModel();
            PageTitle = "Create New Patient";
            if (id.HasValue)
            {
                PageTitle = "Edit Patient";
                var patientView = await _httpClient.GetAsync<PatientViewModel>($"Patient/{id.Value}");
                PatientView = patientView.Body;
            }
            await FillSelectLists();
        }

        public async Task<ActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) { return ModelState.ToJsonError(); }
            if (PatientView.Id == 0)
            {
                await _httpClient.PostAsync<PatientViewModel, PatientViewModel>("Patient", PatientView);
            }
            else
            {
                await _httpClient.PutAsync<PatientViewModel>($"Patient/{PatientView.Id}", PatientView);
            }
            return new JsonResult(PatientView);
        }

        private async Task FillSelectLists()
        {
            var genders = await _httpClient.GetAsync<List<ListMasterViewModel>>($"ListMaster/ByType/{ListType.Gender}");
            PatientView.Genders = new SelectList(genders.Body, "Value", "Text");
            var bloodGroups = await _httpClient.GetAsync<List<ListMasterViewModel>>($"ListMaster/ByType/{ListType.BloodGroup}");
            PatientView.BloodGroups = new SelectList(bloodGroups.Body, "Value", "Text");
            var educationLevels = await _httpClient.GetAsync<List<ListMasterViewModel>>($"ListMaster/ByType/{ListType.Education}");
            PatientView.EducationLevels = new SelectList(educationLevels.Body, "Value", "Text");
            var smokingStatuses = await _httpClient.GetAsync<List<ListMasterViewModel>>($"ListMaster/ByType/{ListType.SmokingStatus}");
            PatientView.SmokingStatuses = new SelectList(smokingStatuses.Body, "Value", "Text");
        }
    }
}