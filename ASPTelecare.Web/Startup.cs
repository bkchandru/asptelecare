using ASPTelecare.Common;
using ASPTelecare.Common.Helper;
using CorrelationId;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Globalization;

namespace ASPTelecare.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration.SetConfiguration(env, true);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            ConfigurationManager.AppSetting = Configuration.Get<AppSetting>();
            services.AddLog();
            services.AddProfiler();
            services.AddExternalHttpServiceSettings();
            services.AddCorrelationId();
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(o => o.LoginPath = new PathString("/Account/login"));

            services.AddAntiforgery(options => options.HeaderName = Constants.AntiforgeryTokenName);

            var mvcBuilder = services.AddMvc(options =>
            {
                options.CacheProfiles.Add("Default", new CacheProfile() { Duration = 60 });
                options.CacheProfiles.Add("Never", new CacheProfile() { Location = ResponseCacheLocation.None, NoStore = true });
            }).AddJsonOptions(opt =>
            {
                opt.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                opt.SerializerSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
                var resolver = opt.SerializerSettings.ContractResolver;
                if (resolver != null)
                {
                    var res = resolver as DefaultContractResolver;
                    res.NamingStrategy = null;  // <<!-- this removes the camelcasing
                }
            })
             .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            AddGlobalization(services, mvcBuilder);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCorrelationId();
            app.UseExceptionHandler(env);
            if (!env.IsDevelopment())
            {
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            UseGlobalization(app);
            app.UseAuthentication();
            app.UseProfiler(env);
            app.UseMvc();
        }

        public void AddGlobalization(IServiceCollection services, IMvcBuilder mvcBuilder)
        {
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            mvcBuilder.AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix);
            //mvcBuilder.AddDataAnnotationsLocalization(options => {
            //    options.DataAnnotationLocalizerProvider = (type, factory) =>
            //        factory.Create(typeof(SharedResource));
            //});
        }

        public void UseGlobalization(IApplicationBuilder app)
        {
            IList<CultureInfo> supportedCultures = new List<CultureInfo>
            {
                new CultureInfo("en-US"),
                new CultureInfo("fi-FI"),
            };
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture("en-US"),
                //DefaultRequestCulture = new RequestCulture("fi-FI"),
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures
            });
        }
    }
}
