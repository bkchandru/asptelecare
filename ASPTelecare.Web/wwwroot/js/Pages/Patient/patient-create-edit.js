﻿var createEditUrl = _baseUrl + "/Patient/CreateEdit/";
var deletelUrl = _baseUrl + "/Patient/delete/";
var searchUrl = _baseUrl + "/Patient/Search";
var searchFields = ["Search_ShowClosed", "Search_JobsForReview"];
var addButton = "<a href=\"" + createEditUrl + "\" class=\"btn btn-primary btn-sm grid-search-button\"><i class=\"fas fa-plus-circle\"></i> Add New</a>";
var gridButtons = [addButton];
$(document).ready(function () {
    var columns = [
        { "title": "First Name", "data": "FirstName", "defaultContent": "" },
        { "title": "Last Name", "data": "LastName", "defaultContent": "" },
        { "title": "Gender", "data": "Gender", "defaultContent": "" },
        { "title": "Date Of Birth", "data": "DateOfBirth", "defaultContent": "" },
        { "title": "Blood Group", "data": "BloodGroup", "defaultContent": "" },
        { "title": "Mobile Number", "data": "MobileNumber", "defaultContent": "" },
        { "title": "EmailId", "data": "EmailId", "defaultContent": "" },
        { "title": "Manage", "data": "Id", "orderable": false, "render": function (data, type, row, meta) { return createActionRow(data, type, row, meta) } },
    ];
    var dataTable = CreateDataTablesGrid("GridControl", columns, searchUrl, null, gridButtons, false);
});

function createActionRow(data, type, row, meta) {
    return '<center><a href="' + createEditUrl + row.Id + '" title=\"Edit\"><i class="fas fa-edit"></i></a> <a href="#" onclick="javascript:deleteRecord(\'' + row.Id + '\',\'' + deletelUrl + '\',\'dataGrid\');" title=\"Delete\"><i class=\"fas fa-trash\"></i></a> </center>';
    //return '<center><a href="' + createEditUrl + row.Id + '" title=\"Edit\"><i class="fas fa-edit"></i></a> <a href="#" onclick="javascript:deleteRecord(\'' + row.Id + '\',\'' + deletelUrl + '\',\'dataGrid\');" title=\"Delete\"><i class="fas fa-trash"></i></a></center>';
}