﻿$(document).ready(function () {
    var createUrl = _baseUrl + "/User/Create/";
    var editUrl = _baseUrl + "/User/Edit/";
    var deletelUrl = _baseUrl + "/User/delete/";
    var searchUrl = _baseUrl + "/User/Search";
    var resetPasswordUrl = _baseUrl + "/User/ResetPassword/";
    var searchFields = ["Search_ShowClosed", "Search_JobsForReview"];
    var addButton = "<a href=\"" + createUrl + "\" class=\"btn btn-primary btn-sm grid-search-button\"><i class=\"fas fa-plus-circle\"></i> Add New</a>";
    var gridButtons = [addButton];

    var columns = [
        { "title": "First Name", "data": "FirstName", "defaultContent": "" },
        { "title": "Last Name", "data": "LastName", "defaultContent": "" },
        { "title": "Username", "data": "Username", "defaultContent": "" },
        { "title": "Email", "data": "Email", "defaultContent": "" },
        { "title": "Role", "data": "Role", "defaultContent": "" },
        { "title": "Active", "data": "Active", "defaultContent": "" },
        { "title": "Last Login Date", "data": "LastLoginDate", "defaultContent": "" },
        { "title": "Manage", "data": "Id", "orderable": false, "render": function (data, type, row, meta) { return createActionRow(data, type, row, meta) } },
    ];
    var dataTable = CreateDataTablesGrid("GridControl", columns, searchUrl, null, gridButtons, false);
});

function createActionRow(data, type, row, meta) {
    return '<center><a href="' + editUrl + row.Id + '" title=\"Edit\"><i class="fas fa-edit"></i></a> <a href="#" onclick="javascript:deleteRecord(\'' + row.Id + '\',\'' + deletelUrl + '\',\'dataGrid\');" title=\"Delete\"><i class=\"fas fa-trash\"></i></a> <a href="#" onclick="javascript:resetPassword(\'' + row.Id + '\',\'' + resetPasswordUrl + '\');" title=\"Reset Password\"><i class=\"fa fa-unlock-alt\"></i></a></center>';
}