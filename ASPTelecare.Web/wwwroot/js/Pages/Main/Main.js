﻿$(document).ready(function () {
    $(".page-content").load("/Patient/DashboardContent", function () {
        LoadScripts({
           
        });
    });
    var objProfile = $("#aProfile");
    var objDashboard = $("#aDashboard").closest("li");
    var objDocOnDemand = $("#aDocOnDemand");
    var objLogout = $("#aLogout");
    var objPayments = $("#aPayments");
    $(objProfile).unbind('click');
    $(objProfile).bind("click", function () {        
        $(".page-content").empty();
        $(".page-content").load("/Patient/Profile", function () {
            $("#aBasicInfo").trigger('click');
            $("#ProfileTabContainer").children(":first").addClass('show').addClass('active');
            LoadScripts({

            });
        });
    });
    $(objDashboard).unbind('click');
    $(objDashboard).bind("click", function () {
        $(".page-content").empty();
        Main_ActivateNavigation(objDashboard);
        $(".page-content").load("/Patient/DashboardContent", function () {
           
        });
    });
    $(objDocOnDemand).unbind('click');
    $(objDocOnDemand).bind("click", function () {
        $(".page-content").empty();
        $(".page-content").load("/Patient/DoctorOnDemand", function () {

        });
    });
    $("#aContact").unbind('click');
    $("#aContact").bind("click", function () {
        $(".page-content").empty();        
        $(".page-content").load("/Patient/ContactInfo", function () {

        });
    });
    $(objPayments).unbind('click');
    $(objPayments).bind("click", function () {
        LoadPayments();
    });
    $("#aSupport").unbind('click');
    $("#aSupport").bind("click", function () {
        $(".page-content").empty();
        $(".page-content").load("/Patient/Support", function () {

        });
    });
});

function LoadPayments() {
    $(".page-content").empty();
    $(".page-content").load("/Patient/Payment", function () {
        $("#btnCreditCard").unbind();
        $("#btnCreditCard").bind('click', function () {
            $(".page-content").empty();
            $(".page-content").load("/Patient/PaymentMethods/CreditCard", function () {
                $("#btnCancelCreditCard").unbind();
                $("#btnCancelCreditCard").bind('click', function () {
                    LoadPayments();
                });
            });
        });
    });
}

function Main_ActivateNavigation(objElement) {
    $(".metisMenu").children().removeClass("active");
    $(objElement).addClass("active");
}