﻿var _baseUrl = window.location.protocol + "//" + window.location.host;//.origin;
$.extend(true, $.fn.dataTable.defaults, {
    "searching": true,
    "ordering": true,
    "processing": true,
    "serverSide": true,
    "searchDelay": 800,
    "info": true,
    "stateSave": false,
    "pagingType": "full_numbers",
    "language": {
        "paginate": {
            "first": "<i class=\"fa fa-fast-backward\"></i>",
            "previous": "<i class=\"fa fa-backward\"></i>",
            "next": "<i class=\"fa fa-forward\"></i>",
            "last": "<i class=\"fa fa-fast-forward\"></i>"
        }
    },
    "pageLength": 10,
    "lengthMenu": [[10, 20, 50, 100, 10000], [10, 20, 50, 100, 'All']],
    "order": [[0, "asc"]],
    "dom": "<'row'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
});

function CreateDataTablesGrid(tableName, columns, searchUrl, searchFields, gridButtons, allowExport, sortOrder, sortIndex) {
    if (sortIndex == null) {
        sortIndex = 0;
    }
    var dataTable = $('#' + tableName).dataTable({
        "serverData": function (sSource, aoData, fnCallback) {
            var sortColIndex = aoData[2].value[0].column;
            var sortDirection = aoData[2].value[0].dir;
            var sortColumn = aoData[1].value[sortColIndex].data;
            aoData.push({ "name": "SortColumn", "value": sortColumn });
            aoData.push({ "name": "SortDirection", "value": sortDirection });
            var fullTextSearchValue = aoData[5].value.value;
            aoData.push({ "name": "Search_FullText", "value": fullTextSearchValue });
            if (searchFields == null) {
                $("[id^='Search_']").each(function (index, element) {
                    aoData.push({ "name": element.id, "value": $(element).val() });
                });
            } else {
                for (var i = 0; i < searchFields.length; i++) {
                    searchField = searchFields[i];
                    aoData.push({ "name": searchField, "value": $("#" + searchField).val() });
                }
            }
            $.ajax({
                "dataType": 'json', "type": "GET", "url": searchUrl, "data": aoData, "success": fnCallback, "error": function (xhr, textStatus, errorThrown) { $('#dataGrid_processing').hide(); var alert = CreateMessageBox(xhr.responseText, 'Error'); $(alert).prependTo($('#' + tableName).parent()).show(); }
            });
        },
        "columns": columns,
        "order": (sortOrder != null) ? [[sortIndex, "desc"]] : [[sortIndex, "asc"]]
    });
    var serach = $("div.dataTables_filter input");
    serach.unbind();
    serach.bind('keypress', function (e) {
        if (e.which == 13) { dataTable.fnFilter(serach.val(), null, false, true); }
    });
    var searchButton = $("<a class=\"btn btn-primary btn-sm grid-search-button\" href=\"#\" id=\"searchButton\"><i class=\"fas fa-search\"></i></a>").insertAfter(serach).on('click', function (e) {
        dataTable.fnFilter(serach.val(), null, false, true);
    });
    if (gridButtons != null) {
        var buttons = "";
        for (i = 0; i < gridButtons.length; i++) {
            buttons += gridButtons[i];
        }
        $(buttons).insertAfter(searchButton);
    }
    return dataTable;
}

function RefreshGrid(gridName) {
    var dataTable = $('#' + gridName).dataTable();
    dataTable.fnDraw();
}

function DeleteRecord(rowId, deleteUrl, gridName) {
    if (confirm("Are you sure want to delete?")) {
        var url = deleteUrl + rowId;
        $.ajax({
            url: url,
            type: "POST",
            async: false,
            cache: false,
            success: function (result) {
                refreshGrid(gridName);
            }
        });
    }
} 

function FormatGridDateRow(data, type, row, meta) {
    if (data != null) {
        return new Date(data).format(_dateFormat);
    } else {
        return ""
    }
}

