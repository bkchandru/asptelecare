﻿var objAppManager = new Object();
var iUniqueCounter = 1;
var iTinyUniqueCounter = 1;
var intTimer;

function SetAppManagerDetails(strWebAPIURL, strBaseUrl, strIsAuthenticated, strSessionKey) {
    objAppManager.WebAPIURL = strWebAPIURL;
    objAppManager.BaseUrl = strBaseUrl;
    objAppManager.IsAuthenticated = strIsAuthenticated;
    objAppManager.SessionKey = strSessionKey;
}

function SetSessionkey() {
    iUniqueCounter = iUniqueCounter + 1;
    var objdate = new Date();
    objAppManager.SessionKey = objdate.getDate().toString() + objdate.getMonth().toString() + objdate.getFullYear().toString() + objdate.getHours().toString() + objdate.getMinutes().toString() + objdate.getSeconds().toString() + iUniqueCounter;
}

function GetUniqueId() {
    ///<summary> Generates a unique id base Date,month,year. Used to avoid browser caching of pages</summary>
    ///<returns Type="Int">Unique Id</returns>
    iTinyUniqueCounter = iTinyUniqueCounter + 5;
    var objdate = new Date()
    return objdate.getMinutes().toString() + objdate.getSeconds().toString() + iTinyUniqueCounter;
}

function LoadCss(strCssFileName, strCssFileUrl) {
    ///<Summary>This Method Loads the Css. if it is not already loaded(checks for the LinkName) then it loads the Css File.</Summary>
    ///<parm name="strCssFileName">Link Name Identifier.</parm>
    ///<parm name="strCssFileUrl">URL of the Css File</parm>    
    var objCssLink = $('head').find("link[LinkName='" + strCssFileName + "']");
    if ($(objCssLink).length <= 0) {
        $('head').append($("<link rel='stylesheet' href='" + strCssFileUrl + "?JSVersionNumber=" + GetUniqueId() + "' LinkName=" + strCssFileName + " type='text/css' />"));
    }
}

function LoadScripts(objScriptJson) {
    ///<Summary>This Method Loads the Scripts with the unique VersionNumber(web.config file) and calls the callback method</Summary>
    ///<parm name="strCssFileName">Link Name Identifier.</parm>
    ///<parm name="strCssFileUrl">URL of the Css File</parm>

    var strScript = "";
    $.each(objScriptJson.ScriptsJSON, function (intScriptIndex, strScriptFile) {
        strScript += "'" + strScriptFile + "'";
        if (intScriptIndex != objScriptJson.ScriptsJSON.length - 1) {
            strScript += ",";
        }
    });
    if (strScript != "") {
        Sys.loadScripts(eval("[" + strScript + "]"), function () {
            if ($.isFunction(objScriptJson.LoadCompleteScripts)) {
                objScriptJson.LoadCompleteScripts()
            };
        });
    }
}

function GetCultureDate(strDate, IsClientSide) {
    var strReturnDate = '';
    if (strDate != '' && strDate != undefined && strDate != null) {
        strDate = new Date(strDate).toISOString().substring(0, 10);
        strDate = strDate.split("-");
        var yyyy = strDate[0];
        var mm = strDate[1];
        var dd = strDate[2];
        //var currentDt = new Date(strDate);
        //var mm = currentDt.getMonth();
        //if (IsClientSide)
        //    mm = (1 + currentDt.getMonth()).toString();
        //var dd = currentDt.getDate();
        //var yyyy = currentDt.getFullYear();
        strReturnDate = mm + '/' + dd + '/' + yyyy;
    }
    return strReturnDate;
}

function GetCultureDateOnEdit(strDate, IsClientSide) {   
    var strReturnDate = '';
    var strDateString = '';
    if (strDate != '' && strDate != undefined && strDate != null) {        
        var currentDt = new Date(strDate);
        var mm = currentDt.getMonth();
        if (IsClientSide)
            mm = (1 + currentDt.getMonth()).toString();
        var dd = currentDt.getDate();
        var yyyy = currentDt.getFullYear();
        strReturnDate = mm + '/' + dd + '/' + yyyy;
        if (mm.toString().length == 1) mm = "0" + mm;
        if (dd.toString().length == 1) dd = "0" + dd;
        strDateString = yyyy + "-" + mm + "-" + dd;       
    }
    //strReturnDate = new Date(strReturnDate).toISOString().substring(0, 10);
    //var strReturnDateArray = strReturnDate.split("-");
    //var strDate = strReturnDateArray[strReturnDateArray.length - 1];
    //var strMonth = strReturnDateArray[1];
    //var sreActualDate = strDate;
    //if (strMonth == "1" && 
    //var strDateString = strReturnDateArray[0] + "-" + strReturnDateArray[1] + "-" + (parseInt(strDate) + parseInt(1));
    
    return strDateString;
}

function GetCultureDateTime(strDate, IsClientSide) {
    var strReturnDate = '';
    if (strDate != '' && strDate != undefined && strDate != null) {
        var currentDt = new Date(strDate);
        var mm = currentDt.getMonth();
        if (IsClientSide)
            mm = (1 + currentDt.getMonth()).toString();
        var dd = currentDt.getDate();
        var yyyy = currentDt.getFullYear();
        var intHours = currentDt.getHours();
        var intMin = currentDt.getMinutes();
        var intSec = currentDt.getSeconds();
        strReturnDate = mm + '/' + dd + '/' + yyyy + " " + intHours + ":" + intMin + ":" + intSec;
    }
    return strReturnDate;
}

function ValidatePageControls(objPageDiv) {
    var blnIsFormValid = true;
    var arrPageControlList = $(objPageDiv).find("[validate='Yes']");
    arrPageControlList.each(function () {
        var boolValid = true;
        if ($(this).is('input:text')) {
            if ($.trim($(this).val()) == "" || $.trim($(this).val()) == null) {
                boolValid = false;
            }
        }
        if ($(this).is('textarea')) {
            if ($.trim($(this).val()) == "" || $.trim($(this).val()) == null) {
                boolValid = false;
            }
        }
        else if ($(this).is("[type='number']")) {
            if ($.trim($(this).val()) == "" || $.trim($(this).val()) == null) {                
                boolValid = false;
            }
            else {
                if (!$.isNumeric($(this).val())) {
                    boolValid = false;
                }
            }
        }
        else if ($(this).is("[type = 'date'][ActualType='Date']")) {
            if ($.trim($(this).val()) == "" || $.trim($(this).val()) == null) {
                boolValid = false;
            }
            else {
                var currVal = GetCultureDate($(this).val(), true);

                var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
                var dtArray = currVal.match(rxDatePattern); // is format OK?
                if (dtArray == null) {
                    boolValid = false;
                }

                //checks for mm/dd/yyyy format
                dtMonth = dtArray[1];
                dtDay = dtArray[3];
                dtYear = dtArray[5];

                if (dtMonth < 1 || dtMonth > 12)
                    boolValid = false;
                else if (dtDay < 1 || dtDay > 31)
                    boolValid = false;
                else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
                    boolValid = false;
                else if (dtMonth == 2) {
                    var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
                    if (dtDay > 29 || (dtDay == 29 && !isleap))
                        boolValid = false;
                }
            }

        }
        else if ($(this).is('select')) {
            if ($(this).val() == -1) {
                boolValid = false;
            }

        }
        else if ($(this).is(':checkbox')) {

        }
        else if ($(this).is(':checkbox')) {

        }
        else if ($(this).is("[type = 'email']")) {
            if ($.trim($(this).val()) == "" || $.trim($(this).val()) == null) {
                boolValid = false;
            }

            var curEmail = $(this).val();
            var reExp = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            if (!reExp.test(curEmail)) {
                boolValid = false;
            }
        }
        else if ($(this).is("[type = 'password']")) {
            if ($.trim($(this).val()) == "" || $.trim($(this).val()) == null) {
                boolValid = false;
            }

            var curPassword = $(this).val();
            if (curPassword.length > 20) {
                boolValid = false;
            }
            //----- regular expression values need to be put in between / and / --
            var reExp = /^[a-zA-Z0-9]*$/;
            if (!reExp.test(curPassword)) {
                boolValid = false;
            }

        }

        if (!boolValid) {
            $(this).addClass("ErrorField");
        }
        else {
            $(this).removeClass("ErrorField");
        }
        if (!boolValid)
            blnIsFormValid = false;
    });
    return blnIsFormValid
}

function AspCares_LoadEssentialData(blnLoad340B, objClientDropdown, obj340BDropdown, OnChange340B, blnShowAll) {
    if (blnShowAll == undefined) blnShowAll = false;
    var objDropdown = $(objClientDropdown);
    var objServiceParams = {
        TargetClass: 'ASPCares.BusinessLogic.Client,ASPCares.340B.BusinessLogic',
        TargetMethod: 'GetAllClients',
        Parameter: { uUserId: objAppManager.UserId },
        CallbackMethod: function (objServiceResponse) {
            $(document).data("Clients" + objAppManager.UserId, objServiceResponse);
            if (!blnShowAll)
                $(objDropdown).html('<option value="-1">Select Covered Entity</option>');
            else
                $(objDropdown).html('<option value="-1">ALL</option>');
            $.each(objServiceResponse, function (key, objClient) {
                $(objDropdown).append('<option value="' + objClient.iClientId + '">' + objClient.vClientName + '</option>');
            });
            $(objDropdown).unbind('change');
            $(objDropdown).bind('change', function (e) {
                //upload
                $(document).find("[Type='SuccessMessage']").hide();
                $(document).find(".uploaded-files-block").hide();
                if (blnLoad340B) {
                    var intClientId = $(e.target).val();
                    AspCares_LoadOnBoarding(obj340BDropdown, OnChange340B, intClientId, blnShowAll);
                }
                else OnChange340B();
                //OnChange340B();
            });
        }
    };
    AspCares_ServiceInvoker(objServiceParams);
}

function AspCares_LoadOnBoarding(objDropdown340B, OnChange340B, intClientId, blnShowAll) {
    if (blnShowAll == undefined) blnShowAll = false;
    var objServiceParams = {
        TargetClass: 'ASPCares.BusinessLogic.OnBoarding,ASPCares.340B.BusinessLogic',
        TargetMethod: 'GetAllOnBoardings',
        Parameter: { uUserId: objAppManager.UserId, iClientId: intClientId },
        CallbackMethod: function (objServiceResponse) {
            $(document).data("340bClient" + intClientId, objServiceResponse);
            if (!blnShowAll)
                $(objDropdown340B).html('<option value="-1">Select 340BID</option>');
            else
                $(objDropdown340B).html('<option value="-1">ALL</option>');
            $.each(objServiceResponse, function (key, objClient) {
                $(objDropdown340B).append('<option value="' + objClient.iOnBoardingId + '">' + objClient.v340BID + '</option>');
            });
            OnChange340B();
            $(objDropdown340B).unbind('change');
            $(objDropdown340B).bind('change', function (e) {
                $(document).find(".uploaded-files-block").hide();
                $(document).find("[Type='SuccessMessage']").hide();
                if (OnChange340B != null) {
                    OnChange340B();
                }
            });
        }
    };
    AspCares_ServiceInvoker(objServiceParams);
}


function GetQueryStringValue(strKey) {
    var strReturn = '';
    var strSplitPageURL = window.location.href.split('?');
    if (strSplitPageURL.length > 1) {
        /* Form the Array of QueryStrings Passed*/
        var strQueryStringParameters = strSplitPageURL[1].split('&');
        $.each(strQueryStringParameters, function (Index, strValue) {
            var strActualValue = strQueryStringParameters[Index].split('=')
            if (strKey == strActualValue[0]) {
                strReturn = strActualValue[1];
                return;
            }
        });
        /****************************************/
    }
    return strReturn;
}

function checkClientTimeZoneName() {
    // Set the client time zone
    var dt = new Date();
    SetCookie("ClientDateTime", dt.toString(), 1);

    var tz = -dt.getTimezoneOffset();
    var tzName = GetTimezoneNameByOffset(tz);


    // Expire in one year
    dt.setYear(dt.getYear() + 1);
    SetCookie("expires", dt.toUTCString(), 1);
}



var _dateFormat = "mm/dd/yyyy";
var _baseUrl = window.location.protocol + "//" + window.location.host;//.origin;
var _sucessMessage = "The record saved successfully!";
var _unAuthorized401MessageTitle = "Unauthorized";
var _unAuthorized401Message = "Sorry, you are not authorized to view the request page. Please contact administrator.";
var _sessionExpiry403MessageTitle = "Forbidden";
var _sessionExpiry403Message = "Sorry, your session has expired. Please login again to continue.";
var _sessionExpiry404MessageTitle = "Not Found";
var _sessionExpiry404Message = "Sorry, the requested file or folder is not found.";
var _sessionExpiry500MessageTitle = "Internal Server Error";
var _sessionExpiry500Message = "We encountered an error while processing your request. We apologies for inconvenience. Please reload the page and try again.<br/>If you continue to encounter this error contact our <a href='mailto:gurubn@hitforward.in?subject=Error:{CORRELATION_ID}&body=Hi,We got an error while acessing your website.'>customer support</a> by mentioning the ID: {CORRELATION_ID}<br/><br/><strong>Error Message:</strong>{ERROR_MESSAGE}";
var _antiforgeryTokenHeaderName = "X-TOKEN-ANTIFORGERY";
var _antiForgeryToken = "";
var _loadingIndicatorElementName = "loading-indicator";
var _loadingIndicatorDelay = 2000;
var _showLoadingIndicator = false;
var _loadingIndicatortimeOutRef;


function InitializeJQueryAjax() {
    HideProgress();
    $.ajaxSetup({
        beforeSend: function (xhr) {
            $('#ServerMessage').hide();
            xhr.setRequestHeader(_antiforgeryTokenHeaderName, _antiForgeryToken);
        },
        error: function (xhr) {
            debugger;
            HideProgress();
            HandleServerError(xhr);
        }
    });

    $(document).ajaxStart(function () {
        _showLoadingIndicator = true;
        _loadingIndicatortimeOutRef = setTimeout("ShowProgress()", _loadingIndicatorDelay);
    });

    $(document).ajaxStop(function () {
        HideProgress();
    });
}

function ShowProgress() {
    if (_showLoadingIndicator) {
        $("#" + _loadingIndicatorElementName).show();
    }
}

function HideProgress() {
    _showLoadingIndicator = false;
    $("#" + _loadingIndicatorElementName).hide();
    clearTimeout(_loadingIndicatortimeOutRef);
}

function classExists(className) {
    if ($('.' + className)[0]) {
        return true;
    }
    return false;
}

function HandleServerError(xhr) {
    debugger;
    var errorMessageTitle = _sessionExpiry500MessageTitle;
    var errorMessage = _sessionExpiry500Message;
    var type = "Warning";
    if (xhr.status == 400 || xhr.status == 422)//Bad Request/Unprocessable Entity
    {
        type = "Warning";
        errorMessageTitle = "Validation Error!";
        errorMessage = HandleValidationError(xhr);
    } else if (xhr.status == 401) {
        type = "Warning";
        errorMessageTitle = _unAuthorized401MessageTitle;
        errorMessage = _unAuthorized401Message;
    } else if (xhr.status == 403) {
        type = "Warning";
        alert(message);
        window.location.href = _loginUrl;
        return;
    } else if (xhr.status == 404) {
        type = "Warning";
        errorMessageTitle = _sessionExpiry404MessageTitle;
        errorMessage = _sessionExpiry404Message;
    } else if (xhr.status == 500) {
        type = "Error";
        errorMessageTitle = _sessionExpiry500MessageTitle;
        var error = JSON.parse(xhr.responseText);
        errorMessage = _sessionExpiry500Message.replace(/{ERROR_MESSAGE}/gi, error.Message);
        errorMessage = errorMessage.replace(/{CORRELATION_ID}/gi, error.CorelationId);
        errorMessage += error.Stack;
    }
    else {
        type = "Error";
        errorMessage = xhr.status + ": " + xhr.statusText;
    }
    var alert = CreateMessageBox(errorMessage, type);
    $(alert).prependTo(_currentForm).show();
}

function CreateMessageBox(message, type) {
    $('#ServerMessage').remove();
    var alertStyle = "alert-warning";
    var icon = "fas fa-exclamation-triangle";
    if (type == "Error") {
        alertStyle = "alert-danger";
        icon = "fas fa-times";
    } else if (type == "Success") {
        alertStyle = "alert-success";
        icon = "fas fa-check-circle";
    } else if (type == "Warning") {
        alertStyle = "alert-warning";
        icon = "fas fa-exclamation-triangle";
    }
    var closeButton = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
    var alert = "<div id=\"ServerMessage\" class=\"alert " + alertStyle + " alert-dismissible fade show\" role=\"alert\"><i class=\"" + icon + "\" style=\"font-size:2rem;\"></i> " + message + closeButton + "</div>";
    return alert;
}