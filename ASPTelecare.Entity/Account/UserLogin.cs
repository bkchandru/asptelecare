﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPTelecare.Entity.Account
{
    [Table("UserLogin", Schema = "Account")]
    public class UserLogin : IdentityUserLogin<int> { }
}
