﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPTelecare.Entity.Account
{
    [Table("UserToken", Schema = "Account")]
    public partial class UserToken : IdentityUserToken<int> { }
}
