﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPTelecare.Entity.Account
{
    [Table("RoleClaim", Schema = "Account")]
    public partial class RoleClaim : IdentityRoleClaim<int> { }
}
