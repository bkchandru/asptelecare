﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPTelecare.Entity.Account
{
    [Table("UserRole", Schema = "Account")]
    public partial class UserRole : IdentityUserRole<int> { }
}
