﻿using ASPTelecare.Common;
using ASPTelecare.Common.Entity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASPTelecare.Entity.Account
{
    [Table("User", Schema = "Account")]
    public class User : IdentityUser<int>, IDbAuditColumns, IDbDeletedColumn, IDbInstituteColumn
    {
        [Required,MaxLength(20), Column(TypeName = "nvarchar(20)")]
        public UserType UserType { get; set; }

        [Required, MaxLength(60)]
        public string FirstName { get; set; }

        [Required, MaxLength(60)]
        public string LastName { get; set; }

        [MaxLength(60)]
        public string MiddleName { get; set; }

        [MaxLength(80)]
        public string DisplayName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [MaxLength(250)]
        public string Note { get; set; }

        public bool Active { get; set; }
        public bool Approved { get; set; }
        [MaxLength(60)]
        public string UpdatedBy { get;set; }
        public DateTime? UpdatedDate { get; set; }
        [MaxLength(60)]
        public string AddedBy { get; set; }
        public DateTime AddedDate { get; set; }
        public bool Deleted { get; set; }
        public int InstituteId { get; set; }
    }
}
