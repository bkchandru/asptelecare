﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPTelecare.Entity.Account
{
    [Table("RolePrivilege", Schema = "Account")]
    public partial class RolePrivilege
    {
        [Key]
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }

        [Key]
        public int PrivilegeId { get; set; }
        public virtual Privilege Privilege { get; set; }
    }
}
