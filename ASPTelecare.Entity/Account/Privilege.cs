﻿using ASPTelecare.Common.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASPTelecare.Entity.Account
{
    [Table("Privilege", Schema = "Account")]
    public partial class Privilege : BaseDbIdColumn<int>
    {
        public int? ParentPrivilegeId { get; set; }
        public virtual Privilege ParentPrivilege { get; set; }

        [Required,MaxLength(60)]
        public string Name { get; set; }

        [MaxLength(250)]
        public string Description { get; set; }

        public short? DisplayOrder { get; set; }

        [MaxLength(250)]
        public string NavigationUrl { get; set; }

        [MaxLength(60)]
        public string ImageCSSName { get; set; }

        public bool Active { get; set; }

        public bool Secured { get; set; }

        public short Level { get; set; }

        [MaxLength(60)]
        public string Target { get; set; }
        public virtual ICollection<RolePrivilege> RolePrivilege { get; set; }

        public short? ApplicationId { get; set; }
        //public virtual Application Application { get; set; }
    }
}
