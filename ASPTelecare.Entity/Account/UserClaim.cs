﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPTelecare.Entity.Account
{
    [Table("UserClaim", Schema = "Account")]
    public partial class UserClaim : IdentityUserClaim<int> { }
}
