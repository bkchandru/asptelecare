﻿using ASPTelecare.Common.Entity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPTelecare.Entity.Account
{
    [Table("Role", Schema = "Account")]
    public partial class Role : IdentityRole<int>, IDbAuditColumns, IDbInstituteColumn
    {
        public virtual ICollection<RolePrivilege> RolePrivilege { get; set; }
        [MaxLength(60)]
        public string UpdatedBy { get; set;}
        public DateTime? UpdatedDate { get; set; }
        [MaxLength(60)]
        public string AddedBy { get; set; }
        public DateTime AddedDate { get; set; }
        public int InstituteId { get; set; }
    }
}
