﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASPTelecare.Entity
{
    [Table("ProviderPatient", Schema = "Institute")]
    public class ProviderPatient
    {
        [Key]
        public int ProviderId { get; set; }
        public virtual Provider Provider { get; set; }
        [Key]
        public int PatientId { get; set; }
        public virtual Patient Patient { get; set; }

    }
}
