﻿using ASPTelecare.Common.Entity;
using ASPTelecare.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace ASPTelecare.Entity
{
    [Table("Provider", Schema = "Institute")]
    public class Provider : BaseDbIdColumn<int>, IDbAuditColumns, IDbDeletedColumn, IDbInstituteColumn
    {
        [Required,MaxLength(60)]
        public string FirstName { get; set; }
        [MaxLength(60)]
        public string MiddleName { get; set; }
        [Required,MaxLength(60)]
        public string LastName { get; set; }
        [Required, MaxLength(20)]
        public string Gender { get; set; }
        [Required, MaxLength(300)]
        public string Qualification { get; set; }
        [Required, MaxLength(50)]
        public string NPI { get; set; }
        [MaxLength(20)]
        public string ProviderType { get; set; }
        [MaxLength(20)]
        public string PreferredLanguage { get; set; }
        [MaxLength(20)]
        public string Speciality { get; set; }
        [MaxLength(500)]
        public string InterStateMedicalLicensure { get; set; }
        public long? FileId { get; set; }
        //public virtual File File { get; set; }
        [Required, MaxLength(15)]
        public string MobileNumber { get; set; }
        [MaxLength(15)]
        public string HomeNumber { get; set; }
        
        [Required,MaxLength(255)]
        public string EmailId { get; set; }
        public int? POBoxNumber { get; set; }
        public int? UserId { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string AddedBy { get; set; }
        public DateTime AddedDate { get; set; }
        public virtual ICollection<Address> Address { get; } = new List<Address>();
        public virtual ICollection<ProviderPatient> ProviderPatients { get; } = new List<ProviderPatient>();
        public int InstituteId { get; set; }
    }
}
