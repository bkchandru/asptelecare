﻿using ASPTelecare.Common.Entity;
using ASPTelecare.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPTelecare.Entity
{
    [Table("Institution", Schema = "Institute")]
    public partial class Institution: BaseDbIdColumn<int>, IDbAuditColumns, IDbDeletedColumn
    {
        public int? ParentInstitutionId { get; set; }
        public virtual Institution ParentInstitution { get; } = new Institution();

        [Required, MaxLength(60)]
        public string Name { get; set; }
        public int? AdministratorId { get; set; }

        [MaxLength(100)]
        public string AdminName { get; set; }

        [MaxLength(2000)]
        public string Hostnames { get; set; }
        public long? FileId { get; set; }
        //public virtual File File { get; set; }
        public int TimeZoneId { get; set; }
        //public virtual TimeZone TimeZone { get; set; }

        [MaxLength(250)]
        public string Note { get; set; }
        [Required, MaxLength(60)]
        public string Code { get; set; }
        public int? IoTInstitutionId { get; set; }
        public bool Active { get; set; }
        
        [MaxLength(60)]
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        [MaxLength(60)]
        public string AddedBy { get; set; }
        public DateTime AddedDate { get; set; }
        public bool Deleted { get; set; }
        public virtual ICollection<Address> Address { get; } = new List<Address>();
        //public virtual ICollection<AccountPolicy> AccountPolicy { get; }
        //public virtual ICollection<DatabaseSetting> DatabaseSetting { get; }
        //public virtual ICollection<InstitutionCulture> InstitutionCulture { get; }
        //public virtual ICollection<TermsOfUse> TermsOfUse { get; }
        //public virtual ICollection<User> User { get;; }
    }
}
