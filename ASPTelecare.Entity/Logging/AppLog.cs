﻿using ASPTelecare.Common.Entity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPTelecare.Entity
{
    [Table("AppLog", Schema = "Logging")]
    public class AppLog : BaseDbIdColumn<long>
    {
        [MaxLength(200)]
        public string CorelationId { get; set; }

        [Required, MaxLength(50)]
        public string Application { get; set; }


        [Required, MaxLength(50)]
        public string Level { get; set; }
        [Required]
        public string Message { get; set; }
        [MaxLength(250)]
        public string Logger { get; set; }

        public string Callsite { get; set; }
        public string Exception { get; set; }
        [MaxLength(15)]
        public string IPAddress { get; set; }
        [MaxLength(5000)]
        public string UserAgent { get; set; }

        [MaxLength(60)]
        public string CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
