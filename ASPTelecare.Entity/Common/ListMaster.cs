﻿using ASPTelecare.Common;
using ASPTelecare.Common.Entity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPTelecare.Entity.Common
{
    [Table("ListMaster", Schema = "Common")]
    public class ListMaster : BaseDbIdColumn<int>, IDbDeletedColumn,IDbInstituteColumn
    {
        [Required, MaxLength(20), Column(TypeName = "nvarchar(20)")]
        public ListType ListType { get; set; }
        [Required, MaxLength(120)]
        public string Value { get; set; }
        [Required, MaxLength(120)]
        public string Text { get; set; }
        public int InstituteId { get; set; }
        public bool Deleted { get; set; }
    }
}
