﻿using ASPTelecare.Common;
using ASPTelecare.Common.Entity;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASPTelecare.Entity.Common
{
    [Table("Address", Schema = "Common")]
    public class Address : BaseDbIdColumn<int>, IDbAuditColumns, IDbDeletedColumn
    {
        [Required, MaxLength(20), Column(TypeName = "nvarchar(20)")]
        public AddressType AddressType { get; set; }
        [Required, MaxLength(120)]
        public string AddressLine1 { get; set; }
        [MaxLength(120)]
        public string AddressLine2 { get; set; }
        [Required, MaxLength(60)]
        public string City { get; set; }
        public int StateId { get; set; }
        public int CountryId { get; set; }
        [MaxLength(50)]
        public string POBoxNumber { get; set; }
        [Required,MaxLength(12)]
        public string Zipcode { get; set; }
        [MaxLength(60)]
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        [MaxLength(60)]
        public string AddedBy { get; set; }
        public DateTime AddedDate { get; set; }
        public bool Deleted { get; set; }
    }
}
